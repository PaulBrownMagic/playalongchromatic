---
title: Brochan Lom
date: 2020-01-13T08:30:13Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/SpNYFKLXpxk.jpg
categories:
 -
tags:
 -
---

{{< youtube SpNYFKLXpxk >}}

Brochan Lom, play along 
lyrics, chords, sheet music, chromatic harmonica tabs

https://playalongmusic.files.wordpress.com/2020/01/brochan-lom.pdf

Words in Scotch Gaelic:  (English below)
Brochan lom, tana lom, brochan lom na sÃ¹ghain
Brochan lom, tana lom, brochan lom na sÃ¹ghain
Brochan lom, tana lom, brochan lom na sÃ¹ghain
Brochan lom 's e tana lom 's e brochan lom na sÃ¹ghain
 
SÃ©ist  (chorus)
Brochan tana, tana, tana, brochan lom na sÃ¹ghain
Brochan tana, tana, tana, brochan lom na sÃ¹ghain
Brochan tana, tana, tana, brochan lom na sÃ¹ghain
Brochan lom 's e tana lom 's e brochan lom na sÃ¹ghain
 
Thugaibh aran dha na gillean leis a' bhrochan sÃ¹ghain
Thugaibh aran dha na gillean leis a' bhrochan sÃ¹ghain
Thugaibh aran dha na gillean leis a' bhrochan sÃ¹ghain
Brochan lom 's e tana lom 's e brochan lom na sÃ¹ghain
 
SÃ©ist (chorus)

Words in English:

Porridge thin and meagre, porridge thin from sowans.
Porridge thin and meagre, porridge thin from sowans.
Porridge thin and meagre, porridge thin from sowans.
Porridge thin, it is meagre and thin, it is porridge thin from sowans.
 
Chorus
Meagre and thin porridge, thin, thin, meagre porridge
Meagre and thin porridge, thin, thin, meagre porridge
Meagre and thin porridge, thin, thin, meagre porridge
Porridge thin, it is meagre and thin, it is porridge thin from sowans.
 
Give ye bread to the young men with sowans-gruel,
Give ye bread to the young men with sowans-gruel,
Give ye bread to the young men with sowans-gruel,
Porridge thin, it is meagre and thin, it is porridge thin from sowans.
 
Chorus

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/01/brochan-lom.pdf)
