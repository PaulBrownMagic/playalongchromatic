---
title: Nights in White Satin
date: 2018-09-15T22:49:00Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/PU_G_qFa3T4.jpg
categories:
 -
tags:
 -
---

{{< youtube PU_G_qFa3T4 >}}

Nights in White Satin, play along 
Words and Music by Justin Hayward 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/09/nights-in-white-satin-2.pdf)
