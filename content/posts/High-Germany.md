---
title: High Germany
date: 2019-04-06T05:43:52Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/xk4la2NH3QY.jpg
categories:
 -
tags:
 -
---

{{< youtube xk4la2NH3QY >}}

High Germany, play along,   Tempo = 120 BPM
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/04/high-germany.pdf)
