---
title: You Raise Me Up
date: 2018-01-16T07:52:17Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/hKRMs9MC7g4.jpg
categories:
 -
tags:
 -
---

{{< youtube hKRMs9MC7g4 >}}

You Raise Me Up  play along 
lyrics, sheet music, chords, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/you-raise-me-up-play-along.pdf)
