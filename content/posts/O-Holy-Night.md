---
title: O Holy Night
date: 2018-12-20T19:48:30Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/dHPTuv6Iab4.jpg
categories:
 -
tags:
 -
---

{{< youtube dHPTuv6Iab4 >}}

O Holy Night, play along
Lyrucs, chords, sheet Music, chromatic Harmonica tabs
Free PDF:  https://playalongmusic.files.wordpress.com/2018/12/O-Holy-Night.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/O-Holy-Night.pdf)
