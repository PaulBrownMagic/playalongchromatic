---
title: Green Grow the Rashes, O
date: 2018-05-25T05:13:06Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/itrv5_gSSI0.jpg
categories:
 -
tags:
 -
---

{{< youtube itrv5_gSSI0 >}}

Green Grow the Rashes, O,  by Robert Burns,
lyrics, chords, sheet music, chromatic harmonica tabs
PDF: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/green-grow-the-rashes-o.pdf)
