---
title: The Butcher Boy
date: 2019-12-02T02:51:35Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/1NRtmTzMO4A.jpg
categories:
 -
tags:
 -
---

{{< youtube 1NRtmTzMO4A >}}

The Butcher Boy, play along, 
PDF:  https://playalongmusic.files.wordpress.com/2019/12/the-butcher-boy.pdf
Traditional folk song, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/12/the-butcher-boy.pdf)
