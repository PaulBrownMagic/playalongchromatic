---
title: Blow the Wind Southerly
date: 2018-05-24T15:33:00Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/16bBYy7iqsc.jpg
categories:
 -
tags:
 -
---

{{< youtube 16bBYy7iqsc >}}

Blow the Wind Southerly, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/blow-the-wind-southerly-pdf1.pdf)
