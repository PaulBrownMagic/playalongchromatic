---
title: Right Here Waiting
date: 2018-04-03T05:33:08Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/yff2WAeKcDk.jpg
categories:
 -
tags:
 -
---

{{< youtube yff2WAeKcDk >}}

Right Here Waiting , play along
chords, lyrics, sheet music, chromatic harmonica tabs
PDF here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/04/right-here-waiting.pdf)
