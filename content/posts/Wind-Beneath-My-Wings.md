---
title: Wind Beneath My Wings
date: 2019-06-15T00:19:28Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/laUzMx6oBG4.jpg
categories:
 -
tags:
 -
---

{{< youtube laUzMx6oBG4 >}}

The Wind Beneath My Wings, by Larry Henley  
and Jeff Silbar 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/06/wind-beneath-my-wings.pdf)
