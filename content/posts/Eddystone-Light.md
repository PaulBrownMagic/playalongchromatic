---
title: Eddystone Light
date: 2018-07-10T01:19:24Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/LsBmQs5fM9A.jpg
categories:
 -
tags:
 -
---

{{< youtube LsBmQs5fM9A >}}

Eddystone Light, play along,
lyrics, chords, sheet music, chromatic harmonica tabs
PDF:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/07/eddystone-light.pdf)
