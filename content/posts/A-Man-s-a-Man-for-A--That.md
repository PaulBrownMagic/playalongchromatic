---
title: A Man’s a Man for A’ That
date: 2018-01-28T03:34:50Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/05CtD_76B6c.jpg
categories:
 -
tags:
 - Celtic
---

{{< youtube 05CtD_76B6c >}}

A Man's a Man for A' That  (play-along) 
by Robert Burns 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/a-mans-a-man-for-a-that.pdf)
