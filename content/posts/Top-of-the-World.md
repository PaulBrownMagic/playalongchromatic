---
title: Top of the World
date: 2019-06-16T03:24:33Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/1MiF2JtXe60.jpg
categories:
 -
tags:
 -
---

{{< youtube 1MiF2JtXe60 >}}

Top of the World, by John Bettis and Richard Carpenter, 
play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/06/top-of-the-world.pdf)
