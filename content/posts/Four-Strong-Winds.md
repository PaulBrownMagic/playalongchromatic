---
title: Four Strong Winds
date: 2018-05-13T05:38:34Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/EKjTm7UBomg.jpg
categories:
 -
tags:
 -
---

{{< youtube EKjTm7UBomg >}}

Four Strong Winds, play along
Words and Music by Ian Tyson
Lyrics, Chords, sheet music, chromatic harmonica tabs
PDF here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/four-strong-winds-play-along.pdf)
