---
title: On Eagle’s Wings
date: 2018-07-09T02:41:03Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/b7BmxBgFdro.jpg
categories:
 -
tags:
 -
---

{{< youtube b7BmxBgFdro >}}

On Eagle's Wings, play along.
lyrics, chords, sheet music, chromatic harmonica tabs
PDF:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/07/on-eagles-wings-1.pdf)
