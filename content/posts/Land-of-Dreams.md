---
title: Land of Dreams
date: 2019-11-28T04:32:26Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/XNugOGKdPVA.jpg
categories:
 -
tags:
 -
---

{{< youtube XNugOGKdPVA >}}

Land of Dreams, play along, 
words and music by Rosanne Cash, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/land-of-dreams.pdf)
