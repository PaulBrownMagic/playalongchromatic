---
title: All the Pretty Little Horses
date: 2018-01-24T05:03:16Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/l68_Dkc7YqM.jpg
categories:
 -
tags:
 - Traditional
 - Lullaby
---

{{< youtube l68_Dkc7YqM >}}

All thr Pretty Little Horses play along 
Key of C 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/all-the-pretty-little-horses.pdf)
