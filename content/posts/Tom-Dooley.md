---
title: Tom Dooley
date: 2020-08-19T21:06:54Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/udTcHUEckSk.jpg
categories:
 -
tags:
 -
---

{{< youtube udTcHUEckSk >}}

Tom Dooley, play along, 
American folk song, 
lyrics, chords, sheet music, chromatic harmonica chords
Free PDF available here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/08/tom-dooley1.pdf)
