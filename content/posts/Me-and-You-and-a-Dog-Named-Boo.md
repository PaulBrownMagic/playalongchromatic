---
title: Me and You and a Dog Named Boo
date: 2018-06-10T00:03:04Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/wRK4cbPskew.jpg
categories:
 -
tags:
 -
---

{{< youtube wRK4cbPskew >}}

Me and You and a Dog Named Boo, play along,
lyrics, cgords, sheet nusic, chromatic harmonica tabs
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/06/me-and-you-and-a-dog-named-boo.pdf)
