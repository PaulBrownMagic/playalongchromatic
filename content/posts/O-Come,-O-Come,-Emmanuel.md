---
title: O Come, O Come, Emmanuel
date: 2019-12-18T07:51:37Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/chLukk8Wd-g.jpg
categories:
 -
tags:
 -
---

{{< youtube chLukk8Wd-g >}}

O Come, O Come, Emmanuel, play long, 
lyrics, chords,sheet music,chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/12/o-come-o-come-emmanuel.pdf)
