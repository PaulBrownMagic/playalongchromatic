---
title: Wha Saw the 42nd
date: 2018-08-01T20:01:35Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/s41bA01LCY0.jpg
categories:
 -
tags:
 -
---

{{< youtube s41bA01LCY0 >}}

Wha Saw the 42nd, play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/08/wha-saw-the-42nd.pdf)
