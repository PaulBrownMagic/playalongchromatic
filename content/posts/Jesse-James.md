---
title: Jesse James
date: 2020-02-28T01:32:50Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/s-DbJ0ShgoQ.jpg
categories:
 -
tags:
 -
---

{{< youtube s-DbJ0ShgoQ >}}

Jesse James (traditional ballad), play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/03/jesse-james.pdf)
