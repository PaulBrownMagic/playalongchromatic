---
title: The Wild Rover
date: 2018-02-13T02:44:20Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/Sx-HnHzh6PM.jpg
categories:
 -
tags:
 -
---

{{< youtube Sx-HnHzh6PM >}}

The Wild Rover Play-Along 
Lyrics, chords, sheet music, chromatic harmonica tabs. 
Downloads PDF here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/the-wild-rover.pdf)
