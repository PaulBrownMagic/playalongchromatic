---
title: Come By the Hills
date: 2019-10-31T04:38:27Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/bQ5WfIGYLkk.jpg
categories:
 -
tags:
 -
---

{{< youtube bQ5WfIGYLkk >}}

Come By the Hills, play along, 
words by W. Gordon Smith, 
music: traditional 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/come-by-the-hills.pdf)
