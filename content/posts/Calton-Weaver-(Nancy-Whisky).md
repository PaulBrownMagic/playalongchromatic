---
title: Calton Weaver (Nancy Whisky)
date: 2020-01-11T01:30:57Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/kRR2VWe-5rw.jpg
categories:
 -
tags:
 -
---

{{< youtube kRR2VWe-5rw >}}

Calton Weaver (Nancy Whisky), play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/01/nancy-whisky.pdf)
