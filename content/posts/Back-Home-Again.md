---
title: Back Home Again
date: 2020-05-02T22:46:44Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/aZUYV_YZfNo.jpg
categories:
 -
tags:
 - Country
 - Folk
 - Rock
---

{{< youtube aZUYV_YZfNo >}}

Back Home Again, play along, 
by John Denver, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/07/back-home-again1.pdf)
