---
title: When I Grow Too Old To Dream
date: 2019-04-01T03:30:06Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/rWWGWeRM3lk.jpg
categories:
 -
tags:
 -
---

{{< youtube rWWGWeRM3lk >}}

When I Grow Too Old to Dream, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs 
words by Oscar Hammerstein 
music by  Sigmund Romberg

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/03/when-i-grow-too-old-to-dream.pdf)
