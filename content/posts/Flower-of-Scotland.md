---
title: Flower of Scotland
date: 2018-07-04T02:41:54Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/38VPx88q6Iw.jpg
categories:
 -
tags:
 -
---

{{< youtube 38VPx88q6Iw >}}

Flower of Scotland, play along 
lryics, chords, sheet music, chromatic harmonica tabs
PDF:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/07/flower-of-scotland.pdf)
