---
title: Blackfly Song
date: 2018-08-02T20:20:06Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/fmGhEOYfFRE.jpg
categories:
 -
tags:
 -
---

{{< youtube fmGhEOYfFRE >}}

The Blackfly Song, play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/08/the-blackfly-song.pdf)
