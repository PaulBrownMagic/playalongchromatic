---
title: Many are the Lightbeams
date: 2019-11-19T00:24:06Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/uSeZSvAw9uA.jpg
categories:
 -
tags:
 -
---

{{< youtube uSeZSvAw9uA >}}

Many Are the Lightbeams, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/many-are-the-lightbeams.pdf)
