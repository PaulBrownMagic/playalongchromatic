---
title: Puff the Magic Dragon
date: 2018-05-28T05:56:57Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/7MoAGtl0FJ4.jpg
categories:
 -
tags:
 -
---

{{< youtube 7MoAGtl0FJ4 >}}

Puff the Magic Dragon, play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/puff-the-magic-dragon.pdf)
