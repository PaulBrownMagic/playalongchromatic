---
title: Christ the Lord is Risen Today
date: 2018-03-30T17:52:55Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/YfTvYxFhx6Y.jpg
categories:
 -
tags:
 -
---

{{< youtube YfTvYxFhx6Y >}}

Christ the Lord is Risen Today by Charles Wesley,
Play Along

Lyrics, chords, sheet music, chromatic harmonica tabs
PDF here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/03/christ-the-lord.pdf)
