---
title: Something to Sing About (This Land of Ours)
date: 2018-06-28T07:19:57Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/NWo67DB5WY0.jpg
categories:
 -
tags:
 -
---

{{< youtube NWo67DB5WY0 >}}

canada flag musical ride, play along,
lyris, chords, sheet music, chromatic harmonica tabs
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/06/something-to-sing-about-this-land-of-ours.pdf)
