---
title: Rolling Down to Old Maui
date: 2019-03-04T07:44:48Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/bZU5rwuY2bQ.jpg
categories:
 -
tags:
 -
---

{{< youtube bZU5rwuY2bQ >}}

Rolling Down to Old Maui, play along
lyrics, chords, sheet music, chromatic harmonica tabs
free PDF here:  https://playalongmusic.files.wordpress.com/2019/03/rolling-down-to-old-maui.pdf

Played on chromatic harmonica by Drifter (Trahciul)  https://audiomack.com/song/trahciul/rolling-down-to-old-maui-harmonica1

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/03/rolling-down-to-old-maui.pdf)
