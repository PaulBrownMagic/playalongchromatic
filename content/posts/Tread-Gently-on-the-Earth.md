---
title: Tread Gently on the Earth
date: 2019-05-17T05:35:23Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/TMHv7mPbbh8.jpg
categories:
 -
tags:
 -
---

{{< youtube TMHv7mPbbh8 >}}

Tread Gently on the Earth, play along 
lyrics, chords, sheet music, chromatic harmonica tabs, 
This song is written by Carolyn Hillyer of https://www.seventhwavemusic.co.uk/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/05/tread-gently-on-the-earth.pdf)
