---
title: One Day at a Time
date: 2020-07-05T06:23:23Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/qtQryABNpxo.jpg
categories:
 -
tags:
 -
---

{{< youtube qtQryABNpxo >}}

One Day at a Time, play along 
by Marijohn Wilkin and Kris Kristofferson, 
Lyrics, chords, sheet music, chromatic harmonica tabs,

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/07/one-day-at-a-time.pdf)
