---
title: Away in a Manger (Murray)
date: 2018-12-05T07:57:08Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/SF72bbdey_s.jpg
categories:
 -
tags:
 - Christmas
 - Hymns
---

{{< youtube SF72bbdey_s >}}

Away in a Manger (Murray), play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/away-in-a-manger-murray.pdf)
