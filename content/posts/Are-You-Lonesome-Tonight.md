---
title: Are You Lonesome Tonight?
date: 2019-04-27T07:38:51Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/R85ZqHsiTdQ.jpg
categories:
 -
tags:
 - Pop
---

{{< youtube R85ZqHsiTdQ >}}

Are You Lonesome Tonight?  by Roy Turk and Lou Handman,  play along, 
lyrics, chords, sheet music, chromatic Harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/04/are-you-lonesome-tonight.pdf)
