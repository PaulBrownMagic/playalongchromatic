---
title: White Cockade
date: 2018-10-07T02:35:39Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/_bELXIf73hw.jpg
categories:
 -
tags:
 -
---

{{< youtube _bELXIf73hw >}}

White Cockade , play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/10/white-cockade.pdf)
