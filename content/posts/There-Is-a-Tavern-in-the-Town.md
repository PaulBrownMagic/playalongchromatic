---
title: There Is a Tavern in the Town
date: 2019-11-24T06:11:40Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/aMCYLS3oZzA.jpg
categories:
 -
tags:
 -
---

{{< youtube aMCYLS3oZzA >}}

There is a Tavern in the Town, play along,
Traditional English drinking song,
lyrics, chords, sheet music, chromatic harmonica tabs
PDF:  https://youtu.be/aMCYLS3oZzA

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/there-is-a-tavern-in-the-town.pdf)
