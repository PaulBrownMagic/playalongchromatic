---
title: Silent Night (duet)
date: 2018-12-18T21:55:42Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/Oix00XIP0j8.jpg
categories:
 -
tags:
 -
---

{{< youtube Oix00XIP0j8 >}}

Silent Night play along duet
lyrics, chords, sheet music, chromatic harmonica tabs

Silent Night PDF:  https://playalongmusic.files.wordpress.com/2018/12/Silent-Night.pdf

2nd Part PDF:    https://playalongmusic.files.wordpress.com/2018/12/Silent-Night-second-part.pdf

More Play Along Videos and PDF's:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/Silent-Night.pdf)
