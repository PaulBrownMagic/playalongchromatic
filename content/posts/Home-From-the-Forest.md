---
title: Home From the Forest
date: 2018-01-30T09:05:06Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/64t2T_GVU08.jpg
categories:
 -
tags:
 -
---

{{< youtube 64t2T_GVU08 >}}

Home From the Forest by Gordon Lightfoot 
Play-along 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/home-from-the-forest.pdf)
