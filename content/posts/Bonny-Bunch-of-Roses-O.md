---
title: Bonny Bunch of Roses O
date: 2019-04-21T20:38:26Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/bd3lORg4d5M.jpg
categories:
 -
tags:
 -
---

{{< youtube bd3lORg4d5M >}}

Bonny Bunch of Roses O, play along video,   Tempo = 100 bpm
lyrics, chords, sheet music, chromatic harmonica tabs

Colm Walsh sings this version of the traditional song:  https://youtu.be/AQE3AS3Vzb0

Free PDF here:  https://playalongmusic.files.wordpress.com/2019/04/the-bonny-bunch-of-roses-o.pdf

More play along videos:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/04/the-bonny-bunch-of-roses-o.pdf)
