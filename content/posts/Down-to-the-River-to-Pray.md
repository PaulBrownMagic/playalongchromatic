---
title: Down to the River to Pray
date: 2018-05-22T00:53:20Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/R1rTUarci6I.jpg
categories:
 -
tags:
 -
---

{{< youtube R1rTUarci6I >}}

Down to the River to Pray, play along
lyrics, chords, sheet music, chromatic harmonica tabs
PDF here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/down-to-the-river-to-pray.pdf)
