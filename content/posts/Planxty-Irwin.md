---
title: Planxty Irwin
date: 2019-02-03T05:52:19Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/JOQ67TgTZ6o.jpg
categories:
 -
tags:
 -
---

{{< youtube JOQ67TgTZ6o >}}

Planxty Irwin by O'Carolan, play along,  BPM 120
chords, sheet music, chromatic harmonica tabs,
PDF here:  https://playalongmusic.wordpress.com

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/02/planxty-irwin.pdf)
