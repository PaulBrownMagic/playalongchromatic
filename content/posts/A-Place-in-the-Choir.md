---
title: A Place in the Choir
date: 2018-11-12T00:21:08Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/NOKpDrESsyU.jpg
categories:
 -
tags:
 - Hymns
---

{{< youtube NOKpDrESsyU >}}

A Place in the Choir, Play along 
written by Bill Staines 
Lyrics, chords, sheet music, chormatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/11/a-place-in-the-choir1.pdf)
