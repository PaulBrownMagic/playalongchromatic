---
title: Camptown Races
date: 2020-08-21T01:07:15Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/qXUuB4G2xwQ.jpg
categories:
 -
tags:
 -
---

{{< youtube qXUuB4G2xwQ >}}

Camptown Races play along,
by Stephen Foster
lyrics, chords, sheet music, chromatic harmonica tabs
Free PDF available here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/08/camptown-races.pdf)
