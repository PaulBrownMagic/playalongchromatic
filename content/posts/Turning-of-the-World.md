---
title: Turning of the World
date: 2018-08-08T08:38:46Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/rItzOKiRJ2E.jpg
categories:
 -
tags:
 -
---

{{< youtube rItzOKiRJ2E >}}

Turning of the World, play along, 
chords, lyrics,sheet music,chromatic harmonica tabs 
 
composed by Ruth Pelam

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/08/turning-of-the-world-key-of-c.pdf)
