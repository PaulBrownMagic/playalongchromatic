---
title: What a Wonderful World
date: 2018-03-21T00:28:44Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/BLAxMCkvuY4.jpg
categories:
 -
tags:
 -
---

{{< youtube BLAxMCkvuY4 >}}

What a Wonderful World. play along,
lyrics, chords, sheet music, chromatic harmonica tabs
PDF here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/03/what-a-wonderful-world.pdf)
