---
title: Eriskay Love Lilt
date: 2019-09-22T19:40:07Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/TmUeF5TC2ws.jpg
categories:
 -
tags:
 -
---

{{< youtube TmUeF5TC2ws >}}

Eriskay Love Lilt, play-along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/09/eriskay-love-lilt.pdf)
