---
title: How Can I Keep From Singing?
date: 2019-04-23T07:36:46Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/Xt_PCPg29ns.jpg
categories:
 -
tags:
 -
---

{{< youtube Xt_PCPg29ns >}}

How Can I Keep From Singing, play aong
Music  by Robert Wadsworth Lowry,
lyrics, chords, sheet music, chromatic harmonica tabs
Free PDF:  https://playalongmusic.files.wordpress.com/2019/04/how-can-i-keep-from-singing.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/04/how-can-i-keep-from-singing.pdf)
