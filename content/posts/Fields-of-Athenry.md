---
title: Fields of Athenry
date: 2018-03-03T05:51:52Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/yaqXdtasG14.jpg
categories:
 -
tags:
 -
---

{{< youtube yaqXdtasG14 >}}

The Fields of Athenry  play along 
lyrics, chords, sheet music, chromatic harmonica tabs 
PDF available here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/03/the-fields-of-athenry.pdf)
