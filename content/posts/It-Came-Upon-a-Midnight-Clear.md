---
title: It Came Upon a Midnight Clear
date: 2018-12-16T08:28:55Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/5O4QW77UCes.jpg
categories:
 -
tags:
 -
---

{{< youtube 5O4QW77UCes >}}

It Came Upon a Midnight Clear, play along
music by Richard Storrs Willis 
lyrics, chords, sheet music, chromatic harmonica tabs

Free PDF here: https://playalongmusic.files.wordpress.com/2018/12/It-Came-Upon-a-Midnight-Clear-2.pdf

More Play Along Videos:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/It-Came-Upon-a-Midnight-Clear-2.pdf)
