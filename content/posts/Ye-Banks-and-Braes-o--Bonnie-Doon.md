---
title: Ye Banks and Braes o’ Bonnie Doon
date: 2019-02-25T04:54:02Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/Gm3i1tYp-5o.jpg
categories:
 -
tags:
 -
---

{{< youtube Gm3i1tYp-5o >}}

Ye Banks and Braes o' Bonnie Doon, play along, 
words by Robbie Burn, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/02/ye-banks-and-braes-o-bonnie-doon.pdf)
