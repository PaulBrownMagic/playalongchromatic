---
title: Smile in Your Sleep (Hush, Hush)
date: 2018-10-03T05:30:33Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/DsKpNXObiMA.jpg
categories:
 -
tags:
 -
---

{{< youtube DsKpNXObiMA >}}

Smile in Your Sleep, play along,
words by Jim McLean, music traditional,
Lyrics, chords, sheet music, chromatic harmonica tabs

Free PDF available here: https://playalongmusic.files.wordpress.com/2018/10/smile-in-your-sleep1.pdf

More play along videos:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/10/smile-in-your-sleep1.pdf)
