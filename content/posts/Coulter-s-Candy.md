---
title: Coulter’s Candy
date: 2018-08-04T19:26:18Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/je-CMtsGRdg.jpg
categories:
 -
tags:
 -
---

{{< youtube je-CMtsGRdg >}}

Coulter's Candy, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/08/coulters-candy.pdf)
