---
title: Joy to the World
date: 2018-12-22T22:35:15Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/HT6Hn5ac1FU.jpg
categories:
 -
tags:
 -
---

{{< youtube HT6Hn5ac1FU >}}

Joy to the World, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs, 
Free PDF here:

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/Joy-to-the-World-1.pdf)
