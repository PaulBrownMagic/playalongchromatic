---
title: Paradise
date: 2020-04-04T06:37:03Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/IqthyRI6U6o.jpg
categories:
 -
tags:
 -
---

{{< youtube IqthyRI6U6o >}}

Paradise by John Prine
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/04/paradise2.pdf)
