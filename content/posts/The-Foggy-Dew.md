---
title: The Foggy Dew
date: 2018-02-10T21:05:24Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/06oxphLZ_qY.jpg
categories:
 -
tags:
 -
---

{{< youtube 06oxphLZ_qY >}}

The Foggy Dew, play-along
lyrics, chords,chromatic harmonica tabs, sheet music
Download pdf here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/the-foggy-dew.pdf)
