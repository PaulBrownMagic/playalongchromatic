---
title: Mary Did You Know
date: 2018-12-12T00:18:01Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/1zs692YqM6w.jpg
categories:
 -
tags:
 -
---

{{< youtube 1zs692YqM6w >}}

Mary Did You Know, Play Along,
lyrics, chords, sheet music, chromatic harmonica tabs
Free PDF available here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/mary-did-you-know-2.pdf)
