---
title: Keep the Home Fires Burning
date: 2020-06-03T07:04:52Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/TykS50_pFk8.jpg
categories:
 -
tags:
 -
---

{{< youtube TykS50_pFk8 >}}

Keep the Home Fires Burning, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/07/keep-the-home-fires-burning1.pdf)
