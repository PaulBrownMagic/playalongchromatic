---
title: Roses of Prince Charlie
date: 2018-01-22T03:19:38Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/dVEUsHOsYZk.jpg
categories:
 -
tags:
 -
---

{{< youtube dVEUsHOsYZk >}}

Roses of Prince Charlie by  Ronnie Browne 
Play-along,  Lyrucs, chords, sheet music, chromatic harmonica tabs
Download PDF here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/roses-of-prince-charlie.pdf)
