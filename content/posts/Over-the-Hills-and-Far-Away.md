---
title: Over the Hills and Far Away
date: 2018-06-05T04:56:40Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/S6YFrWY5z4o.jpg
categories:
 -
tags:
 -
---

{{< youtube S6YFrWY5z4o >}}

Over the Hills and Far Away, play along
lyrics, chords, sheet music, chromatic harmonica tabs
Art by  Pandaren-Chaplain
https://playalongmusic.wordpress.com/

PDF:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/06/over-the-hills-and-far-away1.pdf)
