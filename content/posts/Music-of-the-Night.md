---
title: Music of the Night
date: 2018-01-30T02:38:57Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/TbkoiqVIirQ.jpg
categories:
 -
tags:
 -
---

{{< youtube TbkoiqVIirQ >}}

Music of the Night  Play along 
from Phanton of the Opera 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/music-of-the-night.pdf)
