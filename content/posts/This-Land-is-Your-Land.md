---
title: This Land is Your Land
date: 2019-07-04T03:11:11Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/-RxLy8w-jtw.jpg
categories:
 -
tags:
 -
---

{{< youtube -RxLy8w-jtw >}}

This Land is Your Land, play along, 
by Woody Guthrie, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/07/this-land-is-your-land-1.pdf)
