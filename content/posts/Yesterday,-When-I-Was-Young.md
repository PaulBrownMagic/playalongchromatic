---
title: Yesterday, When I Was Young
date: 2020-09-04T02:22:04Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/EYbKz5nOM8M.jpg
categories:
 -
tags:
 -
---

{{< youtube EYbKz5nOM8M >}}

Yesterday, When I Was Young
lyrics, chords, sheet music, shromatic harmonica tabs.

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/09/yesterday-when-i-was-young.pdf)
