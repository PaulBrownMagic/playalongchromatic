---
title: Sunrise, Sunset
date: 2018-01-23T10:10:27Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/Aotq3bsIix0.jpg
categories:
 -
tags:
 -
---

{{< youtube Aotq3bsIix0 >}}

Sunrise, Sunset, Play-along
From Fiddler on the Roof
Key of Am
Tempo = 100 BPM
chromatic harmonica tabs, lyrics, chords, sheet music

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/10/sunrise-sunset.pdf)
