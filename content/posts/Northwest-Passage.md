---
title: Northwest Passage
date: 2018-05-27T18:22:48Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/PVWMH1TwJsI.jpg
categories:
 -
tags:
 -
---

{{< youtube PVWMH1TwJsI >}}

Northwest Passage, play along
Lyrics, chords, sheet music , chromatic harmonica tabs

PDF:  PDF:   https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/northwest-passage-by-stan-rogers.pdf)
