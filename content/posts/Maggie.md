---
title: Maggie
date: 2019-09-02T20:51:10Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/jC-RBkzDudk.jpg
categories:
 -
tags:
 -
---

{{< youtube jC-RBkzDudk >}}

Maggie (The violets were scenting the woods, Maggie),Nora,

lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/09/maggie.pdf)
