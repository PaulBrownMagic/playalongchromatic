---
title: Crawdad Song
date: 2020-04-22T06:59:34Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/FL129jd0-kU.jpg
categories:
 -
tags:
 -
---

{{< youtube FL129jd0-kU >}}

The Crawdad Song, play along,
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/07/crawdad-song.pdf)
