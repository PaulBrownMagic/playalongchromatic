---
title: Catch the Wind
date: 2019-02-18T20:57:09Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/1XLltfOsk7o.jpg
categories:
 -
tags:
 -
---

{{< youtube 1XLltfOsk7o >}}

Catch the Wind, play along video,  Tempo  120 BPM
words and music by Donovan Leitch,
lyrics, chords, sheet music, chromatic harmonica tabs,

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/02/catch-the-wind.pdf)
