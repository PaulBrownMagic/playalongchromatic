---
title: Colors of the Wind
date: 2018-05-23T01:32:37Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/oQEZhbYuFzQ.jpg
categories:
 -
tags:
 -
---

{{< youtube oQEZhbYuFzQ >}}

Colors of the Wind, play along,
Music by Alan Menken
Words by Stephen Schwartz

Lyrics, chords, sheet music, chromatic harmonica tabs
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/colors-of-the-wind-pdf1.pdf)
