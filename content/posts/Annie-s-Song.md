---
title: Annie’s Song
date: 2018-06-03T01:37:24Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/DbLx8cfNPMo.jpg
categories:
 -
tags:
 - Country
 - Folk
 - Rock
---

{{< youtube DbLx8cfNPMo >}}

Annie's Song, play along,
lyrics, chords, sheet music, chromatic harmonica tabs,

PDF:   https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/06/annies-song1.pdf)
