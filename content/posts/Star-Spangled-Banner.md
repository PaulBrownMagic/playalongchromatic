---
title: Star-Spangled Banner
date: 2020-07-04T07:51:02Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/Of1T26AFBuc.jpg
categories:
 -
tags:
 -
---

{{< youtube Of1T26AFBuc >}}

The Star-Spangled Banner, play along 
words by Francis Scott Key 
 
Lyrics, chords, sheet music, chromatic harmonica tabs 
 
The flag is filmed by Jason S:  
https://youtu.be/75AwcwLz7Wc

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/07/star-spangled-banner.pdf)
