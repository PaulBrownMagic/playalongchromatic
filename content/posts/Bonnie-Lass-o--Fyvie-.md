---
title: Bonnie Lass o’ Fyvie 
date: 2018-05-03T23:51:17Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/gC1UT4t1Kh0.jpg
categories:
 -
tags:
 -
---

{{< youtube gC1UT4t1Kh0 >}}

Bonnie Lass o' Fyvie, play along,
lyrics, chords, sheet music, chromatic harmonica tabs
PDF:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/bonnie-lass-of-fyvie.pdf)
