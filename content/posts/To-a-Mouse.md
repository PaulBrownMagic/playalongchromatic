---
title: To a Mouse
date: 2019-01-18T06:56:30Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/ZHtLIx1wWVg.jpg
categories:
 -
tags:
 -
---

{{< youtube ZHtLIx1wWVg >}}

To a Mouse play-along 
"To a Mouse, on Turning Her Up in Her Nest With the Plough, November, 1785"  by Robert Burns 
Music by Battlefield Band 
 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/01/to-a-mouse.pdf)
