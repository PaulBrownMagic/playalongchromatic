---
title: Sweet Surrender
date: 2018-06-05T00:42:11Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/xBFhCsIMvBY.jpg
categories:
 -
tags:
 -
---

{{< youtube xBFhCsIMvBY >}}

Sweet Surrender, play along,   (written by John Denver)
lyrics, chords, sheet music, chromatic harmonica tabs
PDF:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/06/sweet-surrender-pdf.pdf)
