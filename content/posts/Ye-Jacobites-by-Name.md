---
title: Ye Jacobites by Name
date: 2018-01-25T07:08:49Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/tbJpNq9G_wA.jpg
categories:
 -
tags:
 -
---

{{< youtube tbJpNq9G_wA >}}

Ye Jacobites by Name Play-Along 
lyrics, chords, sheet music, Chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/ye-jacobites.pdf)
