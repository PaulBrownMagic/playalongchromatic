---
title: Castle on a Cloud
date: 2018-09-05T06:54:38Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/wBtHFxxCSA4.jpg
categories:
 -
tags:
 -
---

{{< youtube wBtHFxxCSA4 >}}

Castle on a Cloud, play along
From Les Miserable
lyrics, chords, sheet music, chromatic harmonica tabs

More play along songs here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/09/castle-on-a-cloud.pdf)
