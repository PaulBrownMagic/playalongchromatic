---
title: Evening Prayer
date: 2019-08-03T07:55:21Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/ciGxXygyyKk.jpg
categories:
 -
tags:
 -
---

{{< youtube ciGxXygyyKk >}}

Evening Prayer (Hansel and Gretel)  Tempo = 75 BPM.   
By Engelbert Humperdinck 
lyrics, chords, sheet  music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/08/evening-prayer.pdf)
