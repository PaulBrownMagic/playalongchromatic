---
title: Believe Me If All Those Endearing Young Charms
date: 2019-11-12T07:47:25Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/q2__khPHapE.jpg
categories:
 -
tags:
 -
---

{{< youtube q2__khPHapE >}}

Believe Me If All Those Endearing Young Charms, play along,
lyrics by Thomas Moore,
lyrics, chords, sheet music, chromatic harmonica tabs, 
PDF:  https://playalongmusic.files.wordpress.com/2019/11/believe-me-if-all-those-endearing-youg-charms.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/believe-me-if-all-those-endearing-youg-charms.pdf)
