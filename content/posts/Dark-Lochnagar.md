---
title: Dark Lochnagar
date: 2018-10-02T05:39:04Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/QqjK778ISLg.jpg
categories:
 -
tags:
 -
---

{{< youtube QqjK778ISLg >}}

Dark Lochnagar, play along, 
Words by Lord Byron, music traditional, 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/10/dark-lochnagar.pdf)
