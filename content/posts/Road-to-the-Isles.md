---
title: Road to the Isles
date: 2018-07-15T07:30:42Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/DSKF6opPGkU.jpg
categories:
 -
tags:
 -
---

{{< youtube DSKF6opPGkU >}}

Road to the Isles, play along
lyrics, chords, sheet music, chromatic harmonica tabs

PDF:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/07/road-to-the-isles.pdf)
