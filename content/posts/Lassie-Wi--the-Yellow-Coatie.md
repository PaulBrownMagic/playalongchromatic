---
title: Lassie Wi’ the Yellow Coatie
date: 2019-09-06T07:47:42Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/0FlY9hFz1w0.jpg
categories:
 -
tags:
 -
---

{{< youtube 0FlY9hFz1w0 >}}

Lassie Wi' the Yellow Coatie,
lyrics, chords. sheet music, chromatic harmonica tabs
---------------------------------------------------------------------------------------------------
Lassie Wi the Yellow Coatie

Lassie wi the yellow coatie
Will ye wed a moorland jockie
Lassie wi the yellow coatie
Will ye busk and gang wi me

I ha meal and milk in plenty
I ha kale and cakes fu' dainty
I've a but an ben fu genty
But I want a wife like thee

Lassie wi the yellow coatie
Will ye wed a moorland jockie
Lassie wi the yellow coatie
Will ye busk and gang wi me

 
Tho my mallen be but small
Little gold I hae to show
I've a heart without a flaw
And I'd gie it all tae thee

Lassie wi the yellow coatie
Will ye wed a moorland jockie
Lassie wi the yellow coatie
Will ye busk and gang wi me


Haste ye lassie tae my bossom
While the roses are in blossem
Time is precious, dinna lose them
Flo'ers will fade and so will ye


Lassie wi the yellow coatie
Will ye wed a moorland jockie
Lassie wi the yellow coatie
Will ye busk and gang wi me

sung by Jean Redpath here:
https://youtu.be/wqjGkj1qtTM

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/09/lassie-wi-the-yellow-coatie.pdf)
