---
title: Rising of the Moon
date: 2018-02-25T07:03:01Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/dTHKXH5E9aI.jpg
categories:
 -
tags:
 -
---

{{< youtube dTHKXH5E9aI >}}

Rising of the Moon, play along 
lyrics, chords, sheet music, chromatic harmonica tabs 
PDF available here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/rising-of-the-moon.pdf)
