---
title: Make a Joyful Noise
date: 2019-01-28T01:32:41Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/LUSJan45xdQ.jpg
categories:
 -
tags:
 -
---

{{< youtube LUSJan45xdQ >}}

Make a Joyful Noise (Psalm 100) 
by Linnea Good 
lyrics, chords, chromatic harmonica tabs, sheet music

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/01/make-a-joyful-noise.pdf)
