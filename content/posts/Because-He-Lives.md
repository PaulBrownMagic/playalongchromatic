---
title: Because He Lives
date: 2020-09-07T06:39:07Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/-otAPYgijiE.jpg
categories:
 -
tags:
 -
---

{{< youtube -otAPYgijiE >}}

Because He Lives,  
by Bill and Gloria Gaither
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/09/because-he-lives.pdf)
