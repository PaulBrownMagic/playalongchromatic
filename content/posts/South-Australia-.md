---
title: South Australia 
date: 2020-08-04T03:40:35Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/aVw20biFskM.jpg
categories:
 -
tags:
 -
---

{{< youtube aVw20biFskM >}}

South Australia, play along
lyrics, chords, sheet music, chromatic harmonica tabs
PDF file:  https://playalongmusic.files.wordpress.com/2020/08/south-australia.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/08/south-australia.pdf)
