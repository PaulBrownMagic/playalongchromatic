---
title: Lean on Me
date: 2018-05-05T05:27:05Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/ljVumnVZ_HU.jpg
categories:
 -
tags:
 -
---

{{< youtube ljVumnVZ_HU >}}

Lean on Me, Play Along  by Bill Withers 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/lean-on-me.pdf)
