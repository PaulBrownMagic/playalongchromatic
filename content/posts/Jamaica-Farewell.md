---
title: Jamaica Farewell
date: 2018-12-30T02:49:32Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/JCTJwhY076M.jpg
categories:
 -
tags:
 -
---

{{< youtube JCTJwhY076M >}}

Jamaica Farewell, play along
lyrics, chords, sheet music, chromatic harmonica tabs
PDF:  https://playalongmusic.files.wordpress.com/2018/12/Jamaica-Farewell.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/Jamaica-Farewell.pdf)
