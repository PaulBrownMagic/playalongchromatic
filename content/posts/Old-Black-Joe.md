---
title: Old Black Joe
date: 2020-08-09T00:42:49Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/z-EusLkSiUM.jpg
categories:
 -
tags:
 -
---

{{< youtube z-EusLkSiUM >}}

Old Black Joe, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs
More songs, plus free pdf's, here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/08/old-black-joe.pdf)
