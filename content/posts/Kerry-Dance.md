---
title: Kerry Dance
date: 2019-05-19T02:07:38Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/mQVS3J3oBlk.jpg
categories:
 -
tags:
 -
---

{{< youtube mQVS3J3oBlk >}}

The Kerry Dance , play along,
lyrics, chords, sheet music, chromatic harmonica tabs
Free PDF:  https://playalongmusic.files.wordpress.com/2019/04/the-kerry-dancing.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/04/the-kerry-dancing.pdf)
