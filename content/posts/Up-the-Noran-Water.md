---
title: Up the Noran Water
date: 2019-09-08T05:24:06Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/8FfDRTXlheA.jpg
categories:
 -
tags:
 -
---

{{< youtube 8FfDRTXlheA >}}

Up the Noran Water, 
words:  Helen Cruikshank
music:  Jim Reid
lyrics, chords, sheet music, chromatic harmonica tabs

PDF:  https://playalongmusic.files.wordpress.com/2019/09/up-the-noran-water.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/09/up-the-noran-water.pdf)
