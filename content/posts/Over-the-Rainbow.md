---
title: Over the Rainbow
date: 2018-04-06T05:44:27Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/GbMxZ7XTvhw.jpg
categories:
 -
tags:
 -
---

{{< youtube GbMxZ7XTvhw >}}

Over the Rainbow , play along
chords, lyrics, chromatic harmonica tabs, sheet music
PDF here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/04/over-the-rainbow.pdf)
