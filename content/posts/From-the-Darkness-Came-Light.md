---
title: From the Darkness Came Light
date: 2019-10-27T23:21:01Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/x2m7-ZEr1II.jpg
categories:
 -
tags:
 -
---

{{< youtube x2m7-ZEr1II >}}

From the Darkness Came Light, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/10/from-the-darkness-came-light.pdf)
