---
title: Charlie Is My Darling
date: 2020-02-23T08:28:01Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/nUdryKOBEBQ.jpg
categories:
 -
tags:
 -
---

{{< youtube nUdryKOBEBQ >}}

Charlie Is My Darling, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/03/charlie-is-my-darling-1.pdf)
