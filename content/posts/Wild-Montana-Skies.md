---
title: Wild Montana Skies
date: 2019-11-26T00:53:34Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/UqO-kMs4uHs.jpg
categories:
 -
tags:
 -
---

{{< youtube UqO-kMs4uHs >}}

Wild Montana Skies, play along, 
words and music by John Denver, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/wild-montana-skies.pdf)
