---
title: I’m Gonna Be (500 Miles)
date: 2019-01-10T05:03:42Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/cM-9qAWpXLs.jpg
categories:
 -
tags:
 -
---

{{< youtube cM-9qAWpXLs >}}

I'm Gona Be (500 Miles), play alog video, 
by Charles Reid and Craig Reid 
lyrics, chords, sheet music, chromatic harmonica tabs 
PDF file here:

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/01/im-gonna-be-500-miles.pdf)
