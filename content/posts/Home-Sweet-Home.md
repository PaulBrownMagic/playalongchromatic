---
title: Home Sweet Home
date: 2018-04-24T19:17:38Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/ue_OjlP0wQo.jpg
categories:
 -
tags:
 -
---

{{< youtube ue_OjlP0wQo >}}

Home Sweet Home, play along.
Lyrics, chords, sheet music, chromatic harmonica tabs
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/04/home-sweet-home.pdf)
