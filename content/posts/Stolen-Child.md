---
title: Stolen Child
date: 2018-07-07T06:32:22Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/ZbYZXaTUktE.jpg
categories:
 -
tags:
 -
---

{{< youtube ZbYZXaTUktE >}}

The Stolen Child, play along,
lyrics, chords, sheet music, chromatic harmonica tabs

Lyrics by William Yeats
Music by Loreena McKennitt
PDF:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/07/stolen-child.pdf)
