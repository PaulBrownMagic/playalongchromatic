---
title: Merry Widow Waltz
date: 2019-05-21T04:14:48Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/U6fU-s5WaZI.jpg
categories:
 -
tags:
 -
---

{{< youtube U6fU-s5WaZI >}}

Merry Widow Walts, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/05/merry-widow-waltz.pdf)
