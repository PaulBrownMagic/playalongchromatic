---
title: Do You Hear the People Sing
date: 2018-02-21T03:47:40Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/HtsBLoBTVCs.jpg
categories:
 -
tags:
 -
---

{{< youtube HtsBLoBTVCs >}}

Do You Hear the People Sing? , play-along, from Les Miserables.
Tempo = 80 BPM
The first 9 bars are in the key of F (B flat).  At bar 10 it changes to the key of C.

lyrics, chords, sheet music, chromatic harmonica tabs

https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/do-you-hear-the-people-sing.pdf)
