---
title: The Nightingale
date: 2019-03-16T21:11:41Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/zcD37vjh3AA.jpg
categories:
 -
tags:
 -
---

{{< youtube zcD37vjh3AA >}}

The Nightingale (To hear the Nightingale sing) 
Lyricas, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/03/the-nightingale.pdf)
