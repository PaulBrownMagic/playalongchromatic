---
title: Galway Girl
date: 2018-11-18T22:54:51Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/iFdQn2yvUGo.jpg
categories:
 -
tags:
 -
---

{{< youtube iFdQn2yvUGo >}}

Galway Girl, play along, 
by Steve Earle, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/11/galway-girl.pdf)
