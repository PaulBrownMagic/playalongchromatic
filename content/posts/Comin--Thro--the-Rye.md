---
title: Comin’ Thro’ the Rye
date: 2019-01-20T01:57:24Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/pd02ik0vhfY.jpg
categories:
 -
tags:
 -
---

{{< youtube pd02ik0vhfY >}}

Comin' Thro' the Rye 
words by Robert Burns 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/01/comin-thro-the-rye.pdf)
