---
title: Lock the Door Lariston
date: 2018-04-22T05:19:03Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/-WCLpK5Nv88.jpg
categories:
 -
tags:
 -
---

{{< youtube -WCLpK5Nv88 >}}

Lock the Door Lariston, play along,
lyrics, chords, sheet music, chromatic harmonica tabs
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/04/lock-the-door-lariston.pdf)
