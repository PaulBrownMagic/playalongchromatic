---
title: The Mary Ellen Carter
date: 2019-02-17T02:13:08Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/cbFSwmekh7w.jpg
categories:
 -
tags:
 -
---

{{< youtube cbFSwmekh7w >}}

The Mary Ellen Carter, play along.  Tempo 115 BPM
Words and Music by Stan Rogers
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/02/mary-ellen-carter.pdf)
