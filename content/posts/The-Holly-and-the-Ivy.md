---
title: The Holly and the Ivy
date: 2018-12-06T07:51:58Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/FktwFNqRq1A.jpg
categories:
 -
tags:
 -
---

{{< youtube FktwFNqRq1A >}}

The Holly and the Ivy, Play Along, 
Lyrics, Chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/the-holly-and-the-ivy.pdf)
