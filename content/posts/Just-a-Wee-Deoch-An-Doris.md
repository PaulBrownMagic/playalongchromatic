---
title: Just a Wee Deoch-An-Doris
date: 2018-07-30T17:50:36Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/NRd33detHe8.jpg
categories:
 -
tags:
 -
---

{{< youtube NRd33detHe8 >}}

Just a Wee Deoch An Doris, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/07/wee-deoch-an-doris.pdf)
