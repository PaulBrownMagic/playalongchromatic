---
title: Loch Tay Boat Song
date: 2018-07-14T00:16:10Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/Ji9r_VJGSUQ.jpg
categories:
 -
tags:
 -
---

{{< youtube Ji9r_VJGSUQ >}}

Loch Tay Boat Song , play along, 
lyrics, chords, sheet music, chromatic harmonica tabs
PDF:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/07/loch-tay-boat-song.pdf)
