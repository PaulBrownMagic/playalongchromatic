---
title: Homeward Bound
date: 2020-04-27T17:24:08Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/KKJyZ08qfP0.jpg
categories:
 -
tags:
 -
---

{{< youtube KKJyZ08qfP0 >}}

Homeward Bound, play along, by Paul Simon, 
lyrics, chords, sheet music, chromatic harmonica tabs.

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/07/homeward-bound.pdf)
