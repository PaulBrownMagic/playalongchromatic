---
title: In the Bulb There Is a Flower
date: 2019-06-08T08:06:42Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/y3Jo-KL_-qI.jpg
categories:
 -
tags:
 -
---

{{< youtube y3Jo-KL_-qI >}}

In the Bulb There is a Flower, play along,
by Natalie Sleeth,
lyrics, chords, sheet music, chromatic harmonica tabs

PDF:  https://playalongmusic.files.wordpress.com/2019/06/in-the-bulb-there-is-a-flower.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/06/in-the-bulb-there-is-a-flower.pdf)
