---
title: Comin’ in on a Wing and a Prayer
date: 2020-06-20T02:11:46Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/0pyA2BfdB64.jpg
categories:
 -
tags:
 -
---

{{< youtube 0pyA2BfdB64 >}}

Comin' in on a Wing and a Prayer
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/07/coming-in-on-a-wing-and-a-prayer.pdf)
