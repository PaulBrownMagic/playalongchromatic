---
title: My Nova Scotia Home
date: 2020-09-05T06:10:53Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/XoEAdHcMzME.jpg
categories:
 -
tags:
 -
---

{{< youtube XoEAdHcMzME >}}

My Nova Scotia Home
by Hank  Snow
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/09/my-nova-scotia-home.pdf)
