---
title: I Danced in the Morning
date: 2019-03-29T04:24:06Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/0BfaKNTQepY.jpg
categories:
 -
tags:
 -
---

{{< youtube 0BfaKNTQepY >}}

I Danced in the Morning, play along 
Lyrics, chords,sheet music,  Chromatic harmonica tabs,

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/03/i-danced-in-the-morning.pdf)
