---
title: Colorado Trail
date: 2020-01-19T23:35:57Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/SonUutBkljI.jpg
categories:
 -
tags:
 -
---

{{< youtube SonUutBkljI >}}

Colorado Trail, traditional,  BPM = 90 
lyrics, chords, sheet music, chromatic harmonica tabs
PDF:  https://playalongmusic.files.wordpress.com/2020/01/colorado-trail.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/01/colorado-trail.pdf)
