---
title: This Pretty Planet
date: 2019-04-13T00:45:02Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/POCDTSxdsnI.jpg
categories:
 -
tags:
 -
---

{{< youtube POCDTSxdsnI >}}

This Pretty Planet , play along 
by Tom Chapin and John Forester 
Lyrics, chords, sheet music, chromatic harmonica tabs 
 
PDF available here:  https://playalongmusic.files.wordpress.com/2019/04/this-pretty-planet-tabs.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/04/this-pretty-planet-tabs.pdf)
