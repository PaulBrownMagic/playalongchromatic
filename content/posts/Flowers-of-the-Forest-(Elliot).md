---
title: Flowers of the Forest (Elliot)
date: 2019-11-21T00:01:42Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/vzFyIqer_AI.jpg
categories:
 -
tags:
 -
---

{{< youtube vzFyIqer_AI >}}

Flowers of the Forest, words by Jean Elliot (1727 - 1805),
lyrics, chords, sheet music, chromatic harmonica tabs

as sung by John McDerrmot:  https://youtu.be/aEscHNcvTp0

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/flower-of-the-forest-1.pdf)
