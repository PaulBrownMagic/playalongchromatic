---
title: Bantry Girls’ Lament
date: 2018-10-21T06:09:53Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/QO7tTn-EEM4.jpg
categories:
 -
tags:
 -
---

{{< youtube QO7tTn-EEM4 >}}

Bantry Girls' Lament Play Along Duet
Lyrics, chords, sheet music. chromatic harmonica tabs
PDF here:  https://playalongmusic.files.wordpress.com/2018/10/bantry-girls-lament-duet2.pdf


More Play Along videos here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/10/bantry-girls-lament-duet2.pdf)
