---
title: The Fox 
date: 2018-04-29T01:59:42Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/-ozZLCcixC0.jpg
categories:
 -
tags:
 -
---

{{< youtube -ozZLCcixC0 >}}

The Fox Went Out on a Chilly Night, play along,
With illustrations by Peter Spier,
Lyrics, chords, sheet music, chromatic harmonica tabs
PDF here:   https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/04/the-fox.pdf)
