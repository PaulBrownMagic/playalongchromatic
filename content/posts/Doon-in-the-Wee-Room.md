---
title: Doon in the Wee Room
date: 2018-04-25T18:17:48Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/VJYF_tuF43o.jpg
categories:
 -
tags:
 -
---

{{< youtube VJYF_tuF43o >}}

Doon in the Wee Room, play along,
chords, lyrics, sheet music, cgromatic harmonica tabs
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/04/doon-in-the-wee-room.pdf)
