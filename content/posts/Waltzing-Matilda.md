---
title: Waltzing Matilda
date: 2018-04-27T01:17:09Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/mAQDxOBqLMs.jpg
categories:
 -
tags:
 -
---

{{< youtube mAQDxOBqLMs >}}

Waltzing Matilda, play along
lyrics, chords, sheet music, chromatic harmonica tabs

PDF:  https://playalongmusic.files.wordpress.com/2018/04/waltzing-matilda.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/04/waltzing-matilda.pdf)
