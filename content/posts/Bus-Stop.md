---
title: Bus Stop
date: 2019-07-19T05:40:50Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/q9VTBnPDtsI.jpg
categories:
 -
tags:
 -
---

{{< youtube q9VTBnPDtsI >}}

Bus Stop, play along, 
by Graham Gouldman, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/08/busstop.pdf)
