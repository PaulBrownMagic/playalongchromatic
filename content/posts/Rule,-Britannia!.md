---
title: Rule, Britannia!
date: 2020-08-31T03:36:24Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/r1ikO4jeXfM.jpg
categories:
 -
tags:
 -
---

{{< youtube r1ikO4jeXfM >}}

Rule, Britannia!  play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/08/rule-britannia.pdf)
