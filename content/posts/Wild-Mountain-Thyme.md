---
title: Wild Mountain Thyme
date: 2018-01-07T23:09:37Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/7l0BqJE829I.jpg
categories:
 -
tags:
 -
---

{{< youtube 7l0BqJE829I >}}

Wild Moutain Thyme (with  lead notes played) 
Play along 
Sheet Mudic;  Lyrics; 
Chromatic Harmonica tabs 
made with Band-in-a-Box

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/wild-mountain-thyme.pdf)
