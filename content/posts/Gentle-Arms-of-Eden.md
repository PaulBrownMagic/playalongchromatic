---
title: Gentle Arms of Eden
date: 2018-06-01T01:38:54Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/UbBL7JWEbqY.jpg
categories:
 -
tags:
 -
---

{{< youtube UbBL7JWEbqY >}}

Gentle Arms of Eden , play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/gentle-arms-of-eden.pdf)
