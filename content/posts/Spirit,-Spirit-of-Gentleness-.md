---
title: Spirit, Spirit of Gentleness 
date: 2019-03-30T04:26:30Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/bk5AWccfKPk.jpg
categories:
 -
tags:
 -
---

{{< youtube bk5AWccfKPk >}}

Spirit, Spirit of Gentleness, play along 
by James K. Manley 1975 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/03/spirit-spirit-of-gentleness.pdf)
