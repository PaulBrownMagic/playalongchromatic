---
title: Minstrel Boy
date: 2018-05-14T07:26:15Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/TPs-cY2YrCQ.jpg
categories:
 -
tags:
 -
---

{{< youtube TPs-cY2YrCQ >}}

The Minstrel Boy, play along
lyrics, chords, sheet music, chromatic harmonica tabs
PDF here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/the-minstrel-boy.pdf)
