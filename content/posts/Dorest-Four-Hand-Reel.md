---
title: Dorest Four Hand Reel
date: 2020-06-16T21:59:52Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/Nvz0YnmYwk8.jpg
categories:
 -
tags:
 -
---

{{< youtube Nvz0YnmYwk8 >}}

Dorset Four Hand Reel
chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/07/dorset-four-hond-reel.pdf)
