---
title: Horn of the Hunter
date: 2018-04-28T17:58:10Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/91zgc2mchmo.jpg
categories:
 -
tags:
 -
---

{{< youtube 91zgc2mchmo >}}

Horn of the Hunter, play along
chords, lyrics, sheet music, chromatic harmonica tabs
PDF available here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/04/horn-of-the-hunter.pdf)
