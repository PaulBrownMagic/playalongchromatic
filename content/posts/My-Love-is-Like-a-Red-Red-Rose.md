---
title: My Love is Like a Red Red Rose
date: 2019-01-05T04:32:18Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/V0vG7RYC9oA.jpg
categories:
 -
tags:
 -
---

{{< youtube V0vG7RYC9oA >}}

My Love ia Like a Red Red Rose,
Words by Robert Burns
lyrics, chords, sheet music, chromatic harmonica tabs
Free PDF:  https://playalongmusic.files.wordpress.com/2019/01/my-love-is-like-a-red-red-rose.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/01/my-love-is-like-a-red-red-rose.pdf)
