---
title: Dance to Your Daddy
date: 2018-07-18T06:05:24Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/SyCc-MIoZkM.jpg
categories:
 -
tags:
 -
---

{{< youtube SyCc-MIoZkM >}}

Dance to Your Daddy, play along
Lyrics, chords, sheet music, chromatic harmonica tabs
PDF:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/07/dance-to-your-daddy.pdf)
