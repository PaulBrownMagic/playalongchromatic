---
title: God Hath Not Promised
date: 2019-09-28T06:12:11Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/hUmGVE-O_hg.jpg
categories:
 -
tags:
 -
---

{{< youtube hUmGVE-O_hg >}}

God Hath Not Promised , play along, 
words by Annie Johnson Flint  
music by Grace Note Brothers 
lyrics, chords,sheet music, chromatic harmonica tabs 
source: https://www.hymnal.net/en/hymn/nt/720

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/10/god-hath-not-promised.pdf)
