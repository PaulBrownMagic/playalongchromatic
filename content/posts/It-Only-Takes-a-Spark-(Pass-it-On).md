---
title: It Only Takes a Spark (Pass it On)
date: 2019-06-18T01:21:23Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/r-yMvnxDj18.jpg
categories:
 -
tags:
 -
---

{{< youtube r-yMvnxDj18 >}}

It Only Takes a Spark (Pass It On), play along, 
by Kurt Kaiser, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/06/it-only-takes-a-spark.pdf)
