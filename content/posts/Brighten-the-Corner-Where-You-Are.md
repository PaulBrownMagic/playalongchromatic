---
title: Brighten the Corner Where You Are
date: 2020-03-24T23:26:16Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/B0qkXHjIkZc.jpg
categories:
 -
tags:
 -
---

{{< youtube B0qkXHjIkZc >}}

Brighten the Corner Where You Are, plat along,
music :  Charles Hutchinson Gabriel (1856-1932),
words:   Ina Mae Duley Ogdon, (1872-1964),
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/03/brighten-the-corner-where-you-are.pdf)
