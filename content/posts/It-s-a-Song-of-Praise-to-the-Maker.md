---
title: It’s a Song of Praise to the Maker
date: 2019-05-27T19:24:30Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/1aYWvDm0Ad8.jpg
categories:
 -
tags:
 -
---

{{< youtube 1aYWvDm0Ad8 >}}

It's a Song of Praise to the Maker, play along, 
words : Ruth Duck,  Music : Ron Klusmeier 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/05/its-a-song-of-praise-to-the-maker.pdf)
