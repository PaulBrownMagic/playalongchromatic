---
title: Buffalo Gals
date: 2020-08-10T07:56:59Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/lJd83ij9dkY.jpg
categories:
 -
tags:
 -
---

{{< youtube lJd83ij9dkY >}}

Buffalo Gals, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs
More songs, plus free pdf's, here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/08/buffalo-gals.pdf)
