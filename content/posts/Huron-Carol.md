---
title: Huron Carol
date: 2018-12-02T06:50:14Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/H5ztxXO8BPE.jpg
categories:
 -
tags:
 -
---

{{< youtube H5ztxXO8BPE >}}

Huron Carol, play along
lyrics, chords, sheet music, chromatic harmonica tabs
PDF here: https://playalongmusic.files.wordpress.com/2018/12/huron-carol.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/huron-carol.pdf)
