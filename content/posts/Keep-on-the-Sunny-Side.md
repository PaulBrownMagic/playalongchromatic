---
title: Keep on the Sunny Side
date: 2018-01-12T09:45:55Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/GQFQispeSQs.jpg
categories:
 -
tags:
 -
---

{{< youtube GQFQispeSQs >}}

Keep on the Sunny Side Key of C 
Play along 
sheet music, chords, lyrics 
Chromatic Harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/keep-on-the-sunny-side.pdf)
