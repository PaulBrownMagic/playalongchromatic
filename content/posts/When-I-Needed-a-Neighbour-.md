---
title: When I Needed a Neighbour 
date: 2019-12-05T06:56:01Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/MFmRHiFl5LA.jpg
categories:
 -
tags:
 -
---

{{< youtube MFmRHiFl5LA >}}

When I Needed a Neighbour, play along, 
by Sydney Carter, 
lyrics, chords,sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/12/when-i-needed-a-neighbour.pdf)
