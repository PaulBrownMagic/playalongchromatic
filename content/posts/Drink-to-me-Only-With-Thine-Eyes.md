---
title: Drink to me Only With Thine Eyes
date: 2018-04-27T03:34:19Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/71njOGuDKhA.jpg
categories:
 -
tags:
 -
---

{{< youtube 71njOGuDKhA >}}

Drink to me Only With Thine Eyes, play along
lyrics, chords, sheet music, chromatic harmonica tabs

PDF here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/04/drink-to-me-only-with-thine-eyes.pdf)
