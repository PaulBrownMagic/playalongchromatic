---
title: Pachelbel’s Canon
date: 2018-03-09T23:09:57Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/ha1KbTwr_tw.jpg
categories:
 -
tags:
 -
---

{{< youtube ha1KbTwr_tw >}}

Pachelbel's Canon, Play Along
sheet music, chords, chromatic harmonica tabs

Sheet music PDF here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/03/pachelbels-canon.pdf)
