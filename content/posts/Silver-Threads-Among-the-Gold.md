---
title: Silver Threads Among the Gold
date: 2020-03-09T01:08:38Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/9aG1yuXS8yY.jpg
categories:
 -
tags:
 -
---

{{< youtube 9aG1yuXS8yY >}}

Silver Threads Among the Gold, play along,
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/03/silver-threads-among-the-gold.pdf)
