---
title: Bluebells of Scotland
date: 2019-06-30T06:37:24Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/UOL3_bwsANY.jpg
categories:
 -
tags:
 -
---

{{< youtube UOL3_bwsANY >}}

The Bluebells of Scotland,play along, 
lyrics, chords, sheet nusic, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/06/the-bluebells-of-scotland.pdf)
