---
title: In the Bleak Midwinter (Holst)
date: 2018-12-08T06:57:31Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/V76boCmmPDE.jpg
categories:
 -
tags:
 -
---

{{< youtube V76boCmmPDE >}}

In the Bleak Midwinter, play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/in-he-bleak-midwinter-holst.pdf)
