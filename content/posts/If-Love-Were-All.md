---
title: If Love Were All
date: 2019-06-25T06:46:58Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/a3sSIiNho5k.jpg
categories:
 -
tags:
 -
---

{{< youtube a3sSIiNho5k >}}

If Love Were All, play along, 
written by Noel Coward, from the operette "Bitter Sweet". 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/06/if-love-were-all-refrain.pdf)
