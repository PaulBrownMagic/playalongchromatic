---
title: A Scottish Soldier
date: 2018-01-26T07:01:06Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/9Q0da2Lyhww.jpg
categories:
 -
tags:
 - Celtic
---

{{< youtube 9Q0da2Lyhww >}}

A Scottish Soldier (The Green Hills of Tyrol) 
Play along 
Tempo = 80 BPM 
sheet music, lyrics, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/a-scottish-soldier.pdf)
