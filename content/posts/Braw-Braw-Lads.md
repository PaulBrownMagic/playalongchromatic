---
title: Braw Braw Lads
date: 2018-08-03T04:53:51Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/4YkAkKr5RJU.jpg
categories:
 -
tags:
 -
---

{{< youtube 4YkAkKr5RJU >}}

Braw Braw Lads, play along, 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/08/braw-braw-lads.pdf)
