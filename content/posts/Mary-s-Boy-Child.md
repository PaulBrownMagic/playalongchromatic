---
title: Mary’s Boy Child
date: 2019-12-17T00:14:43Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/K9uKBf2UV80.jpg
categories:
 -
tags:
 -
---

{{< youtube K9uKBf2UV80 >}}

Mary's Boy Child, play-along, 
words and music by Jester Hairston, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/12/marys-boy-child.pdf)
