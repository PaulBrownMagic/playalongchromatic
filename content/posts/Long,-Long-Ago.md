---
title: Long, Long Ago
date: 2019-09-24T07:13:03Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/DUrhaoJ8zCA.jpg
categories:
 -
tags:
 -
---

{{< youtube DUrhaoJ8zCA >}}

Long, Long Ago, play-along,
by Thomas Haynes Bayly,
lyrics, chords, sheet music, chromatic harmonica tabs

PDF:  https://playalongmusic.files.wordpress.com/2019/09/long-long-ago.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/09/long-long-ago.pdf)
