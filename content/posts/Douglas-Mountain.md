---
title: Douglas Mountain
date: 2018-12-14T05:14:40Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/pwHI_oGHkO8.jpg
categories:
 -
tags:
 -
---

{{< youtube pwHI_oGHkO8 >}}

Douglas Mountain, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs 
 
Free PDF here:

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/Douglas-Mountain.pdf)
