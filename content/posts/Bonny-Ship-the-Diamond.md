---
title: Bonny Ship the Diamond
date: 2018-01-28T20:53:14Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/Fo67HScj8k8.jpg
categories:
 -
tags:
 -
---

{{< youtube Fo67HScj8k8 >}}

Bonny Ship the Diamond, play-along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/the-bonny-ship-the-diamond.pdf)
