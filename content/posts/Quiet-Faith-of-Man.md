---
title: Quiet Faith of Man
date: 2018-06-30T09:10:05Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/wRAOfAkNVUk.jpg
categories:
 -
tags:
 -
---

{{< youtube wRAOfAkNVUk >}}

Quiet Faith of Man, play along
words and music by Bill Staines
Lyrics, chords, sheet music, chromatic harmonica tabs
https://playalongmusic.wordpress.com/

PDF here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/06/quiet-faith-of-man.pdf)
