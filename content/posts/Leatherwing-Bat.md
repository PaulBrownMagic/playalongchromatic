---
title: Leatherwing Bat
date: 2018-09-16T23:58:29Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/J54SDztvyhQ.jpg
categories:
 -
tags:
 -
---

{{< youtube J54SDztvyhQ >}}

Leatherwing Bat, play along 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/09/leatherwing-bat.pdf)
