---
title: So Long, It’s Been Good to Know You
date: 2019-04-07T18:45:33Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/FULaySDPHF8.jpg
categories:
 -
tags:
 -
---

{{< youtube FULaySDPHF8 >}}

So Long It's Been Good to Know You, play along  Tempo = 140 BPM
by Woodie Guthrie
Lyrics, chords, sheet music, chromatic harmonica tabs

Free PDF:  https://playalongmusic.files.wordpress.com/2019/04/so-long-its-been-good-to-know-you.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/04/so-long-its-been-good-to-know-you.pdf)
