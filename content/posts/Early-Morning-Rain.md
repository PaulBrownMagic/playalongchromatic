---
title: Early Morning Rain
date: 2018-03-11T08:45:12Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/-wVuhDu6ils.jpg
categories:
 -
tags:
 -
---

{{< youtube -wVuhDu6ils >}}

Early Morning Rain by Gordon Lightfoot, play along
lyrics, chords, sheet music, chromatic harmonica tabs

PDF here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/03/early-morning-rain.pdf)
