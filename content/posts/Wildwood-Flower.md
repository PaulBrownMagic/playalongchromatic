---
title: Wildwood Flower
date: 2020-01-04T02:20:42Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/2ee1IMZL0jo.jpg
categories:
 -
tags:
 -
---

{{< youtube 2ee1IMZL0jo >}}

Wildwood Flower, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/01/wildwood-flower.pdf)
