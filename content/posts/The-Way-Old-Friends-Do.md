---
title: The Way Old Friends Do
date: 2019-07-14T05:10:18Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/IYZO03WSRXY.jpg
categories:
 -
tags:
 -
---

{{< youtube IYZO03WSRXY >}}

The Way Old Friends Do, play along,   
Key of F, Tempo = 62 BPM, 
by Benny Andersson & Bjorn Ulvaeus (of ABBA), 
lyrics, chords, sheet music, chromayic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/07/the-way-old-friends-are.pdf)
