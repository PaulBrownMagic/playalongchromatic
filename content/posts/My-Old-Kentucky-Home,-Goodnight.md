---
title: My Old Kentucky Home, Goodnight
date: 2018-05-02T22:42:37Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/0OZkI0aHeLE.jpg
categories:
 -
tags:
 -
---

{{< youtube 0OZkI0aHeLE >}}

My Old Kentucky Home, Goodnight, play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/my-old-kentucky-home.pdf)
