---
title: Once in Royal David’s City
date: 2019-12-13T06:28:09Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/RW6gBJZNH-4.jpg
categories:
 -
tags:
 -
---

{{< youtube RW6gBJZNH-4 >}}

Once In Royal David's City, play along  Tempo =83 BPM, 
lyrics, chords, sheet music, chromatic harmonica tabs, 
words: Cecil Francis Alexander 1848; 
music: Henry John Gauntlett 1849, 
descant (verse 4): David Willcock 1970

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/12/once-in-royal-davids-city.pdf)
