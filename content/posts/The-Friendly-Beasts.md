---
title: The Friendly Beasts
date: 2019-12-20T21:08:36Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/8Ho3yLWYmMw.jpg
categories:
 -
tags:
 -
---

{{< youtube 8Ho3yLWYmMw >}}

The Friendly Beasts, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/12/the-friendly-beasts.pdf)
