---
title: Twa Recruiting Sergeants
date: 2018-02-05T03:23:30Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/MC2KjHgTAec.jpg
categories:
 -
tags:
 -
---

{{< youtube MC2KjHgTAec >}}

Twa Recruiting Sergeants play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/twa-recruiting-sergeants.pdf)
