---
title: The Road to Dundee
date: 2018-01-18T08:46:47Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/EcD3p4bujdI.jpg
categories:
 -
tags:
 -
---

{{< youtube EcD3p4bujdI >}}

The Road to Dundee play along 
lyrics, chords, sheet music, chromatic harmonica tab

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/road-to-dundee.pdf)
