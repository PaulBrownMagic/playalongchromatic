---
title: Old Toy Trains
date: 2018-11-29T23:28:01Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/0expZlNv2dw.jpg
categories:
 -
tags:
 -
---

{{< youtube 0expZlNv2dw >}}

Old Toy Trains, play along, 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/11/old-toy-trains.pdf)
