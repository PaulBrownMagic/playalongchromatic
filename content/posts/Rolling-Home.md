---
title: Rolling Home
date: 2019-11-16T06:25:45Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/13zUJPztj0k.jpg
categories:
 -
tags:
 -
---

{{< youtube 13zUJPztj0k >}}

Rolling Home, play along,  Tempo = 80 BPM
music: traditional,
words written by  Charles MacKay "On board the Europa, homeward bound, May 26 1858.",
lyrics, chords, sheet music, chromatic harmonica tabs, 
PDF:  https://playalongmusic.files.wordpress.com/2019/11/rolling-home.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/rolling-home.pdf)
