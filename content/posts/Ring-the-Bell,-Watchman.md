---
title: Ring the Bell, Watchman
date: 2019-08-28T06:10:49Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/W5ojWPb2Zjg.jpg
categories:
 -
tags:
 -
---

{{< youtube W5ojWPb2Zjg >}}

Ring the Bell, Watchmen, play along.  Tempo = 95 BPM
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/08/ring-the-bell-watchman.pdf)
