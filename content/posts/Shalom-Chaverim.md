---
title: Shalom Chaverim
date: 2020-08-24T02:07:57Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/W13nJvsBmnU.jpg
categories:
 -
tags:
 -
---

{{< youtube W13nJvsBmnU >}}

Shalom Chaverim, play along,  
lyrics, chords, sheet music, chromatic harmonica tabs
Free pdf file available here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/08/shalom-chaverim.pdf)
