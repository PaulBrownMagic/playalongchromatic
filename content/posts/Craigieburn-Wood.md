---
title: Craigieburn Wood
date: 2018-08-31T18:42:13Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/0BnnytCBLUw.jpg
categories:
 -
tags:
 -
---

{{< youtube 0BnnytCBLUw >}}

Craidgieburn Wood, play along
lyrics, chords, sheet music, chromatic harmonica tabs
PDF file here: https://playalongmusic.files.wordpress.com/2018/08/craidgieburn-wood.pdf
More Play Along Videos and PDF files here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/08/craidgieburn-wood.pdf)
