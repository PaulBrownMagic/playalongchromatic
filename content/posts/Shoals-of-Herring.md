---
title: Shoals of Herring
date: 2018-02-03T06:48:35Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/hMFwumwN-Tk.jpg
categories:
 -
tags:
 -
---

{{< youtube hMFwumwN-Tk >}}

Shoals of Herring play along 
lyrics, chords, sheet music, chromatic harmonica chords

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/sholas-of-herring.pdf)
