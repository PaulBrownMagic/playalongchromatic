---
title: Greenland Whale Fisheries
date: 2019-10-29T19:50:33Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/4fsscdeS3Os.jpg
categories:
 -
tags:
 -
---

{{< youtube 4fsscdeS3Os >}}

Greenland Whale Fisheries, play along,
lyrics, chords, sheet music, chromatic harmonica tabs

PDF here:  https://playalongmusic.files.wordpress.com/2019/10/greenland-whale-fisheries.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/10/greenland-whale-fisheries.pdf)
