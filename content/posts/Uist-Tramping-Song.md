---
title: Uist Tramping Song
date: 2018-07-16T22:42:17Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/wNTPhKMwrFE.jpg
categories:
 -
tags:
 -
---

{{< youtube wNTPhKMwrFE >}}

Uist Tramping Song, play along,
lyrics, chords, sheet music, chromatic harmonica tabs
PDF:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/07/uist-trampinf-song.pdf)
