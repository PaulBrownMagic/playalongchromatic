---
title: Kisses Sweeter Than Wine
date: 2018-05-05T20:21:47Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/b8PFsdlPLjg.jpg
categories:
 -
tags:
 -
---

{{< youtube b8PFsdlPLjg >}}

Kisses Sweeter Than Wine, Play Along
Lyrics, chords,sheet music, Chromatic harmonica tabs
Words by Paul Campbell ( The Weavers), music by Huddie Ledbetter

https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/kisses-sweeter-than-wine.pdf)
