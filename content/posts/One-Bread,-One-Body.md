---
title: One Bread, One Body
date: 2019-03-14T05:15:06Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/g4yzS-EcGgE.jpg
categories:
 -
tags:
 -
---

{{< youtube g4yzS-EcGgE >}}

One Bread, One Body, play along.  
by John Foley
lyrics, chords, sheet music, chromatic harmonica tabs

Played on chromatic Harmonica by Drifter (Trahciul):  https://audiomack.com/song/trahciul/seek-ye-first-harmonica

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/03/one-bread-one-body.pdf)
