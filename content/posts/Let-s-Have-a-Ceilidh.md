---
title: Let’s Have a Ceilidh
date: 2018-04-25T01:47:14Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/qJjArjuuIMc.jpg
categories:
 -
tags:
 -
---

{{< youtube qJjArjuuIMc >}}

Let's Have a Ceilidh, play along
chords, lyrics, sheet music, chromatic harmonica tabs

PDF: https://playalongmusic.files.wordpress.com/2018/04/lets-have-a-ceilidh1.pdf
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/04/lets-have-a-ceilidh1.pdf)
