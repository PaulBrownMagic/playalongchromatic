---
title: Rose of Allendale
date: 2018-06-21T06:15:41Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/5DktINrITJo.jpg
categories:
 -
tags:
 -
---

{{< youtube 5DktINrITJo >}}

Rose of Allendale,  play along,
lyrics, chords, sheet music, chromatic harmonica tabs
PDF here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/06/rose-of-allendale.pdf)
