---
title: Henry Martin
date: 2019-02-10T00:15:35Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/lWbAxXINpAE.jpg
categories:
 -
tags:
 -
---

{{< youtube lWbAxXINpAE >}}

Henry Martin, play along,   BPM:  130
lyrics, chords, sheet music, chromatic harmonica tabs,

PDF:  https://playalongmusic.files.wordpress.com/2019/02/henry-martin.pdf

More Play Along Videos here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/02/henry-martin.pdf)
