---
title: Prairie Spring
date: 2018-07-01T07:06:42Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/wySZQ5hswHk.jpg
categories:
 -
tags:
 -
---

{{< youtube wySZQ5hswHk >}}

Prairie Spring play along
composed by  Jay Ungar
chords, sheet music, chromatic harmonica tabs
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/07/prairie-spring.pdf)
