---
title: Barrett’s Privateers
date: 2018-06-20T03:38:26Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/pWOkdCBok3E.jpg
categories:
 -
tags:
 -
---

{{< youtube pWOkdCBok3E >}}

Barrett's Privateers. play along
written by Stan Rogers
Lyrics, chords, sheet music, chromatic harmonica tabs
https://playalongmusic.wordpress.com/

PDF available here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/06/barretts-privateers1.pdf)
