---
title: Lamb of God
date: 2019-04-20T16:31:41Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/a2LyGwP67JQ.jpg
categories:
 -
tags:
 -
---

{{< youtube a2LyGwP67JQ >}}

Lamb of God, by Twila Paris, play along,
lyrics, chords, sheet music, chromatic harmonica tabs
PDF here  https://playalongmusic.files.wordpress.com/2019/04/lamb-of-god.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/04/lamb-of-god.pdf)
