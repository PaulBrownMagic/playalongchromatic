---
title: Lo, How a Rose E’er Blooming
date: 2019-12-14T08:43:00Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/vYBY8His2yA.jpg
categories:
 -
tags:
 -
---

{{< youtube vYBY8His2yA >}}

Lo How a Rose E're Blooming, play along, 
Traditional German Carol, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/12/lo-how-a-rose-ere-blooming.pdf)
