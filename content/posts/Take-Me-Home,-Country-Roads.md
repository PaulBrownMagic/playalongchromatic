---
title: Take Me Home, Country Roads
date: 2018-02-15T09:24:29Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/ZSeopFH6Ifs.jpg
categories:
 -
tags:
 -
---

{{< youtube ZSeopFH6Ifs >}}

Take Me Home, Country Roads Play-Along 
Lyrics, chords, sheet music, chromatic harmonica tabs 
 
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/take-me-home-country-roads1.pdf)
