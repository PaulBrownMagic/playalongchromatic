---
title: Danny Boy
date: 2018-02-11T21:18:23Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/zocZSHUPqVM.jpg
categories:
 -
tags:
 -
---

{{< youtube zocZSHUPqVM >}}

Danny Boy Play Along 
sheet music, lyrics, chords, chromatic harmonica tabs 
Downladable pdf here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/danny-boy.pdf)
