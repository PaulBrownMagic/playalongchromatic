---
title: Winds of Change
date: 2018-02-01T05:39:17Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/PMGSVaEwJtU.jpg
categories:
 -
tags:
 -
---

{{< youtube PMGSVaEwJtU >}}

Winds of Change  Play-Along 
lyrics, chords, sheetmusic, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/winds-of-change.pdf)
