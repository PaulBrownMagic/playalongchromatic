---
title: Church in the Wildwood
date: 2020-08-22T19:57:39Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/ilDcjNZekZE.jpg
categories:
 -
tags:
 -
---

{{< youtube ilDcjNZekZE >}}

The Church in the Wildwood
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/08/church-in-the-wildwood.pdf)
