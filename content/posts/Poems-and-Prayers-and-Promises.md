---
title: Poems and Prayers and Promises
date: 2018-06-01T07:11:44Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/ZS9fAw9rBG8.jpg
categories:
 -
tags:
 -
---

{{< youtube ZS9fAw9rBG8 >}}

Poems and Prayers and Promises, play along,
written by John Denver,
lyrics, chords, sheet music, chromatic harmonics tabs

PDF:   https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/06/poems-and-prayers-and-promises.pdf)
