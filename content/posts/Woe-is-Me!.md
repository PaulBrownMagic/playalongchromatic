---
title: Woe is Me!
date: 2019-09-01T01:25:14Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/UpfquqszfU4.jpg
categories:
 -
tags:
 -
---

{{< youtube UpfquqszfU4 >}}

"Woe Is Me!" , aka ,"Sad Song on World's Smallest Violin",  by Richard Myhill, as played by Mr. Krabs in the SpongeBob Squarepants Episode "Squilliam Returns", 
 
Chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/08/woe-is-me.pdf)
