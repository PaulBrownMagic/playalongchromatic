---
title: Sound the Pibroch
date: 2019-11-03T06:36:03Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/nSkrrZ4cuvA.jpg
categories:
 -
tags:
 -
---

{{< youtube nSkrrZ4cuvA >}}

Sound the Pibroch, play along, 
words by Agnes Maxwell MacLeod, 
lyrics, chords, sheet music, chromatic harmonica tabs,lyrics, chords, sheet music, chromatic harmonica tabs 
Note: The phrase âtha tighin fodhamâ is pronounced âha cheen fo-umâ, and means âit comes upon me" or "I have the wish"

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/sound-the-pibroch.pdf)
