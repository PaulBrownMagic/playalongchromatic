---
title: Liverpool Lou
date: 2019-05-20T07:05:59Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/v57XsxSbELA.jpg
categories:
 -
tags:
 -
---

{{< youtube v57XsxSbELA >}}

Liverpool Lou play along 
Lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/05/liverpool-lou.pdf)
