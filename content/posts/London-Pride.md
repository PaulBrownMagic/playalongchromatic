---
title: London Pride
date: 2019-06-03T19:18:11Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/nXW5ActNJmc.jpg
categories:
 -
tags:
 -
---

{{< youtube nXW5ActNJmc >}}

London Pride by Noel Coward, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/06/london-pride-2.pdf)
