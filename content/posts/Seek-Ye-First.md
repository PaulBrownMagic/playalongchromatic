---
title: Seek Ye First
date: 2019-03-11T06:24:21Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/4xX9SyKd72k.jpg
categories:
 -
tags:
 -
---

{{< youtube 4xX9SyKd72k >}}

Seek Ye First, play along 
by Karen Lafferty 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/03/seek-ye-first.pdf)
