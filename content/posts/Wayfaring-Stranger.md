---
title: Wayfaring Stranger
date: 2018-02-17T21:33:11Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/t8wia3hLJ4I.jpg
categories:
 -
tags:
 -
---

{{< youtube t8wia3hLJ4I >}}

Wayfaring Stranger Play-Along 
Lyrics, chords, sheet music, chromatic harmonica tabs 
PDF available here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/wayfaring-stranger.pdf)
