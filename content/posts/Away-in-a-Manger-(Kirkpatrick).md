---
title: Away in a Manger (Kirkpatrick)
date: 2018-12-05T07:49:50Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/mqxXVCKV1vo.jpg
categories:
 -
tags:
 - Christmas
 - Hymns
---

{{< youtube mqxXVCKV1vo >}}

Away in a Manger (Kirkpatrick)  Play Along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/away-in-a-manger-kirkpatrick1.pdf)
