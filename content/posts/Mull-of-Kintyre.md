---
title: Mull of Kintyre
date: 2018-06-24T06:35:48Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/QmbQylQydy0.jpg
categories:
 -
tags:
 -
---

{{< youtube QmbQylQydy0 >}}

Mull of Kintyre, play along,
lryics, chords, sheet music, chromatic harmonica tabs
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/06/mull-of-kintyre-1.pdf)
