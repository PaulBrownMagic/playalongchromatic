---
title: I Heard the Bells on Christmas Day
date: 2018-12-19T07:27:56Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/gBN52TJ9nzY.jpg
categories:
 -
tags:
 -
---

{{< youtube gBN52TJ9nzY >}}

I Heard the Bells on Christmas Say, play along
Music by Johnny Marks
lyrics, chords, sheet music, chromatic harmonica tabs
PDF:  https://playalongmusic.files.wordpress.com/2018/12/I-Heard-the-Bells-on-Christmas-Day.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/I-Heard-the-Bells-on-Christmas-Day.pdf)
