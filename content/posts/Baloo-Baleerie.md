---
title: Baloo Baleerie
date: 2019-11-08T04:35:22Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/H8uvBdx5XkA.jpg
categories:
 -
tags:
 - Traditional
 - Lullaby
---

{{< youtube H8uvBdx5XkA >}}

Baloo Baleerie, play along
lyrics, chords, sheet music, chromatic harmonica tabs, PDF
PDF:  https://playalongmusic.files.wordpress.com/2019/11/baloo-baleerie.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/baloo-baleerie.pdf)
