---
title: Ashokan Farewell
date: 2019-01-25T04:46:48Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/vJoXwcf3UIc.jpg
categories:
 -
tags:
 - Folk
---

{{< youtube vJoXwcf3UIc >}}

Ashokan Farewell, play along 
composed by Jay Ungar, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/01/ashokan-farewell.pdf)
