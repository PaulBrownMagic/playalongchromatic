---
title: Highland Cathedral
date: 2019-12-07T04:37:53Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/cLDrXZG4Msg.jpg
categories:
 -
tags:
 -
---

{{< youtube cLDrXZG4Msg >}}

Highland Cathedral, play along, 
Lyrics, chords, sheet music, chromatic harmarnica tabs, 
by MICHAEL KORB and ULRICH ROEVER, 
Lyrics by BEN KELLY,

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/12/highland-cathedral-1.pdf)
