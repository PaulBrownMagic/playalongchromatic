---
title: Like a Healing Stream
date: 2019-11-10T07:55:51Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/ZIFfMQ8c5zc.jpg
categories:
 -
tags:
 -
---

{{< youtube ZIFfMQ8c5zc >}}

Like a Healing Stream, play along, 
Written by Bruce Harding, 
Lyrics, chords,sheet music, chromatic harmonics tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/like-a-healing-stream-1.pdf)
