---
title: Five Hundred Miles
date: 2019-01-12T02:22:35Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/o7gp6ObXpjE.jpg
categories:
 -
tags:
 -
---

{{< youtube o7gp6ObXpjE >}}

Five Hundred Miles play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/01/500-miles.pdf)
