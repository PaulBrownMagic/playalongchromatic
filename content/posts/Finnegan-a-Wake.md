---
title: Finnegan’a Wake
date: 2020-07-06T08:05:07Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/qHqSRuS60dE.jpg
categories:
 -
tags:
 -
---

{{< youtube qHqSRuS60dE >}}

Finnegan's Wake , play along, 
lyrics, chords, sheet music , chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/07/finnegans-wake1.pdf)
