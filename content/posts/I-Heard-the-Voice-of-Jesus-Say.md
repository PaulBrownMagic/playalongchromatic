---
title: I Heard the Voice of Jesus Say
date: 2018-11-14T07:03:07Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/YXsCatLidsg.jpg
categories:
 -
tags:
 -
---

{{< youtube YXsCatLidsg >}}

I Heard the Voice of Jesus Say, play along,
lyrics, chords, sheet music, chromatic harmonica tabs

More play along videos and PDFs  here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/11/i-heard-the-voice-of-jesus-say.pdf)
