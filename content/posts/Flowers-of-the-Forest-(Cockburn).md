---
title: Flowers of the Forest (Cockburn)
date: 2019-11-20T23:46:29Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/IuQ1-iCtsKs.jpg
categories:
 -
tags:
 -
---

{{< youtube IuQ1-iCtsKs >}}

Flowers of the Forest, words by Alison Rutherford Cockburn (1712  - 1794),
lyrics, chords, sheet music, chromatic harmonica tabs, PDF
Tempo = 55 BPM
sung by John McDermott:  https://youtu.be/aEscHNcvTp0

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/flowers-of-the-forest.pdf)
