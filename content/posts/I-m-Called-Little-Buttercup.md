---
title: I’m Called Little Buttercup
date: 2018-09-12T05:50:44Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/yE16hrjzyz4.jpg
categories:
 -
tags:
 -
---

{{< youtube yE16hrjzyz4 >}}

I'm Called Little Buttercup, play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/09/im-called-little-buttercup-2.pdf)
