---
title: Swing Low, Sweet Chariot
date: 2018-03-13T21:09:07Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/UTv9E3n1fCM.jpg
categories:
 -
tags:
 -
---

{{< youtube UTv9E3n1fCM >}}

Swing Low, Sweet Chariot, play along 
Lyrics, chords, sheet music, chromatic harmonica tabs 
 
PFG available here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/03/swing-low-sweet-chariot.pdf)
