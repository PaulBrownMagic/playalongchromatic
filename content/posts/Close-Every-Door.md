---
title: Close Every Door
date: 2018-01-20T05:49:37Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/McR9qsiwh9U.jpg
categories:
 -
tags:
 -
---

{{< youtube McR9qsiwh9U >}}

Close Every Door
from Joseph and the Amazing Technicolor Dreamcoat
 Key of G, Sounded lead, sheet music, chords, lyrics, chromatic harmonica tabs
Tempo - 90 BPM

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/10/close-every-door-to-me.pdf)
