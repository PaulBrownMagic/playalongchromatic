---
title: Down By the Brazos
date: 2019-09-21T05:08:28Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/f7ZJx6yKHhE.jpg
categories:
 -
tags:
 -
---

{{< youtube f7ZJx6yKHhE >}}

Down By the Brazos, play-along
lyrics, chords, sheet music, chromatic harmonica tabs
PDF:  https://playalongmusic.files.wordpress.com/2019/09/down-by-the-brozos.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/09/down-by-the-brozos.pdf)
