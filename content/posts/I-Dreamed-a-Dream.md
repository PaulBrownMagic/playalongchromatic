---
title: I Dreamed a Dream
date: 2018-02-05T04:50:59Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/uuzHvg7SGzE.jpg
categories:
 -
tags:
 -
---

{{< youtube uuzHvg7SGzE >}}

I Dreamed a Dream Play along
lyrica, chords, sheet music, chromatic harmonica tabs

Art by Johnathan Burton  https://handsomefrank.com/illustrators/jonathan-burton/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/i-dreamed-a-dream.pdf)
