---
title: Both Sides Now
date: 2018-06-25T07:18:18Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/5AnpPet-rlE.jpg
categories:
 -
tags:
 -
---

{{< youtube 5AnpPet-rlE >}}

Both Sides Now, play along,
lyrics, chords, sheet music, chromatic harmonica tabs
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/06/both-sides-now1.pdf)
