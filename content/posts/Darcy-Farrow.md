---
title: Darcy Farrow
date: 2019-10-03T06:33:21Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/H_nEl2byCTI.jpg
categories:
 -
tags:
 -
---

{{< youtube H_nEl2byCTI >}}

Darcy Farrow, play along, 
Words and music by Steve Gillette and Tom Campbell, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/10/darcy-farrow.pdf)
