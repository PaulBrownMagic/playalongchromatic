---
title: The Spinning Wheel
date: 2018-10-28T08:21:47Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/U3tP_vJr-ew.jpg
categories:
 -
tags:
 -
---

{{< youtube U3tP_vJr-ew >}}

The Spinning Wheel, Play along
by  John Francis Waller,
Lyrics, Chords, sheet music, chromatic harmonica tabs

Free PDF available here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/10/the-spinning-wheel.pdf)
