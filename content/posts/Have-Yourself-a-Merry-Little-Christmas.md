---
title: Have Yourself a Merry Little Christmas
date: 2018-12-03T05:36:42Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/yJRlxP0E_Sg.jpg
categories:
 -
tags:
 -
---

{{< youtube yJRlxP0E_Sg >}}

Have Yourself a Merry Little Christmas, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/have-yourself-a-merry-little-christmas.pdf)
