---
title: If We Hold On Together Play Along
date: 2018-02-07T08:42:29Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/k564UQ8nJw8.jpg
categories:
 -
tags:
 -
---

{{< youtube k564UQ8nJw8 >}}

If We Hold On Together - play along
from Land Before Time
lyrics, chords, sheet music, chromatic harmonica tabs
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/if-we-hold-on-together.pdf)
