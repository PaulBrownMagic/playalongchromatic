---
title: Feed the Birds
date: 2019-07-16T05:33:22Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/WM92iczD-_c.jpg
categories:
 -
tags:
 -
---

{{< youtube WM92iczD-_c >}}

Feed the Birds, play along,
Tempo = 90 BPM,
by Richard M. Sherman and Robert B. Sherman,
from "Mary Poppins", 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/07/feed-the-birds.pdf)
