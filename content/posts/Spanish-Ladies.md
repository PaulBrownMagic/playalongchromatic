---
title: Spanish Ladies
date: 2020-07-17T04:15:34Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/kNDrs1HYcvg.jpg
categories:
 -
tags:
 -
---

{{< youtube kNDrs1HYcvg >}}

Spanish Ladies, play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/07/spanish-ladies.pdf)
