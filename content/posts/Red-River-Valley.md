---
title: Red River Valley
date: 2019-10-26T06:30:01Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/0IHuwTFm2l0.jpg
categories:
 -
tags:
 -
---

{{< youtube 0IHuwTFm2l0 >}}

Red River Valley, play along,
lyrics, chords, sheet music, chromatic harmonica tabs

PDF here:  https://playalongmusic.files.wordpress.com/2019/10/red-river-valley.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/10/red-river-valley.pdf)
