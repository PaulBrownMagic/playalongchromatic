---
title: The Sun Whose Rays
date: 2019-07-09T16:55:06Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/nETzjG86otA.jpg
categories:
 -
tags:
 -
---

{{< youtube nETzjG86otA >}}

The Sun Whose Rays Are All Ablaze, play along,
by W.S.Gilbert & Sir Arthur Sullivan,
lyics, chords, sheet music, chromatic harmonica tabs

PDF:  https://playalongmusic.files.wordpress.com/2019/07/the-sun-whose-rays.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/07/the-sun-whose-rays.pdf)
