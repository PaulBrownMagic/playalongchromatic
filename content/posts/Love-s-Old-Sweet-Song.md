---
title: Love’s Old Sweet Song
date: 2018-11-06T06:07:51Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/dj6q_YvoHC4.jpg
categories:
 -
tags:
 -
---

{{< youtube dj6q_YvoHC4 >}}

Love's Old Sweet Song, Play Along 
Lyrics, Chords, Sheet Music. Chromatic Hatmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/11/loves-old-sweet-song.pdf)
