---
title: Catch Another Butterfly
date: 2019-10-09T23:01:26Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/flFCGFKAXVY.jpg
categories:
 -
tags:
 -
---

{{< youtube flFCGFKAXVY >}}

Catch Another Butterfly, play along, 
by Michael Williams, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/catch-another-butterfly.pdf)
