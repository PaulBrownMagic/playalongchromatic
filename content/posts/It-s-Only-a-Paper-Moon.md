---
title: It’s Only a Paper Moon
date: 2020-08-21T06:57:03Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/XVChdl492n8.jpg
categories:
 -
tags:
 -
---

{{< youtube XVChdl492n8 >}}

It's Only a Paper Moon, play along
Music: Harold Arlen,  Words: Billy Rose and Yip Harburg,
lyrics, chords, sheet music, chromatic harmonica tabs
Free PDF available here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/08/its-only-a-paper-moon.pdf)
