---
title: Aweigh Santy Ano
date: 2019-10-24T05:45:37Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/rxM_NG1fwd8.jpg
categories:
 -
tags:
 - Shanty
---

{{< youtube rxM_NG1fwd8 >}}

Aweigh,, Santy Ano, play along
Lyrics, chords, sheet music, chromatic harmonica tabs
PDF here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/aweigh-santy-ano.pdf)
