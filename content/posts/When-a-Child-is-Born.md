---
title: When a Child is Born
date: 2018-11-29T05:17:01Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/BMnXKSXTPVY.jpg
categories:
 -
tags:
 -
---

{{< youtube BMnXKSXTPVY >}}

When a Child is Born, play along,
Lyrics, chords, sheet music, chromatic harmonica tabs
PDF here:  https://playalongmusic.files.wordpress.com/2018/11/when-a-child-is-born.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/11/when-a-child-is-born.pdf)
