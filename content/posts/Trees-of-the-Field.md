---
title: Trees of the Field
date: 2018-02-24T20:26:31Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/3-ykRdwUolo.jpg
categories:
 -
tags:
 -
---

{{< youtube 3-ykRdwUolo >}}

Trees of the Field, play along
(You Shall Go Out with Joy)
lyrics, chords, sheet music, chromatic harmonica tabs

PDF available here:

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/trees-of-the-field.pdf)
