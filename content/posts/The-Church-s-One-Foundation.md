---
title: The Church’s One Foundation
date: 2018-04-08T20:03:00Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/Q9l83_ILRUM.jpg
categories:
 -
tags:
 -
---

{{< youtube Q9l83_ILRUM >}}

The Church's One Foundation, play along
lyrics, chords, chromatic harmonica tabs, sheet music
PDF here: https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/04/the-churchs-one-foundation.pdf)
