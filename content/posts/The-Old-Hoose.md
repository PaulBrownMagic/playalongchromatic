---
title: The Old Hoose
date: 2020-05-14T23:55:08Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/soxwkcvWb90.jpg
categories:
 -
tags:
 -
---

{{< youtube soxwkcvWb90 >}}

The Auld Hoose, play along,
words by Lady Carolina Nairne, about her birthplace in Gaask, Perthshire,
lyrics, chords, sheet music, chromatic harmonica tabs,

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/07/the-old-hoose.pdf)
