---
title: Skye Boat Song
date: 2017-12-31T07:48:57Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/0_MJDP5tH8Y.jpg
categories:
 -
tags:
 -
---

{{< youtube 0_MJDP5tH8Y >}}

Skye Boat Song Key of C, play-along 
Sheet Music 
Chromatic Harmonica Tabs 
Band-in-a-Box Style "Esquire  Celtic Slow Waltz"

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/skye-boat-song-key-of-c.pdf)
