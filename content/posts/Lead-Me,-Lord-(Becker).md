---
title: Lead Me, Lord (Becker)
date: 2019-06-12T08:43:39Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/o9t6YRLnbCk.jpg
categories:
 -
tags:
 -
---

{{< youtube o9t6YRLnbCk >}}

Lead Me, Lord, play along, 
by John D. Becker, 
lyrics, chords, sheet music, chromatic harmonica chords,

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/06/lead-me-lord.pdf)
