---
title: Take Me Back
date: 2018-08-20T05:12:51Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/6uo9NNb9p4o.jpg
categories:
 -
tags:
 -
---

{{< youtube 6uo9NNb9p4o >}}

Take Me Back, play along 
lyrics, chords, sheet music, chromatic harmonica tabs 
 
By Andy Stewert

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/08/take-me-back.pdf)
