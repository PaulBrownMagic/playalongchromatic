---
title: Vilja Song
date: 2019-05-25T07:32:09Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/x4M3Suy2A4c.jpg
categories:
 -
tags:
 -
---

{{< youtube x4M3Suy2A4c >}}

Vilja Song, play along 
by Franz Lehar, 
lyrics, chords, sheet music,chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/05/vilja-song.pdf)
