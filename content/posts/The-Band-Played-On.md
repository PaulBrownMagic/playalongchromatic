---
title: The Band Played On
date: 2019-11-09T05:07:32Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/VmmevcCnDrA.jpg
categories:
 -
tags:
 -
---

{{< youtube VmmevcCnDrA >}}

The Band Played On, play along 
lyrics, chords, sheet music, chromatic harmonica tab

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/11/the-band-played-on.pdf)
