---
title: Good Christian Men Rejoice
date: 2019-12-15T23:17:00Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/4XVqpTXFenI.jpg
categories:
 -
tags:
 -
---

{{< youtube 4XVqpTXFenI >}}

Good Christian Men Rejoice, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/12/good-christian-men-rejoice.pdf)
