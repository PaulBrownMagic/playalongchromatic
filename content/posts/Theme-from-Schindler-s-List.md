---
title: Theme from Schindler’s List
date: 2018-09-21T05:41:44Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/BvJ1ds1P3Go.jpg
categories:
 -
tags:
 -
---

{{< youtube BvJ1ds1P3Go >}}

Theme from Schindler's List, play along 
chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/09/theme-from-schindlers-list-2.pdf)
