---
title: When I Was Wandering in the World
date: 2020-01-18T00:38:47Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/-c-BwOw5nTw.jpg
categories:
 -
tags:
 -
---

{{< youtube -c-BwOw5nTw >}}

When I Was Wandering in the World, play along, 
Music by Cheolho Ahn, 
Chords, sheet music, chromatic harmonica chords

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/01/when-i-was-wondering-in-the-world.pdf)
