---
title: Lochs o’ the Tay
date: 2019-09-15T02:36:10Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/G9Por9KtaS4.jpg
categories:
 -
tags:
 -
---

{{< youtube G9Por9KtaS4 >}}

Lochs o' the Tay, play along, by Jim Malcolm, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/09/lochs-o-the-tay.pdf)
