---
title: Like a Rock
date: 2018-10-09T05:27:48Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/u5SEN7EtZvU.jpg
categories:
 -
tags:
 -
---

{{< youtube u5SEN7EtZvU >}}

Like a Rock, play along, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/10/like-a-rock.pdf)
