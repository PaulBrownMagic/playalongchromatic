---
title: River Running in You and Me
date: 2019-06-04T06:03:32Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/tRTKpEZ9cgA.jpg
categories:
 -
tags:
 -
---

{{< youtube tRTKpEZ9cgA >}}

River Running in You and Me (River Run Deep), play along, 
by Ian Macdonald and Gordon Light, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/06/river-running-in-you-and-me.pdf)
