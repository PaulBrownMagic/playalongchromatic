---
title: The Rose
date: 2018-06-26T20:59:20Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/XCyMLWd57Oc.jpg
categories:
 -
tags:
 -
---

{{< youtube XCyMLWd57Oc >}}

The Rose, play along,
lyrics, chords, sheet music, chromatic harmonica tabs
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/06/the-rose-1.pdf)
