---
title: Que Sera, Sera
date: 2019-05-14T17:23:49Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/4wK3FmCvPcM.jpg
categories:
 -
tags:
 -
---

{{< youtube 4wK3FmCvPcM >}}

Que Sera Sera ,play along 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/05/que-sera-sera.pdf)
