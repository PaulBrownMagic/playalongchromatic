---
title: Shores of Botany Bay
date: 2018-05-08T02:44:15Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/tbFsQHvbNGI.jpg
categories:
 -
tags:
 -
---

{{< youtube tbFsQHvbNGI >}}

The Shores of Botany Bay, play along
lyrics, chords, sheet music, chromatic harmonica tabs

PDF here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/05/shores-of-botany-bay.pdf)
