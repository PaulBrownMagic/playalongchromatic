---
title: One More Step Along the World I Go
date: 2019-12-03T08:25:23Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/5ic-M7CsA9E.jpg
categories:
 -
tags:
 -
---

{{< youtube 5ic-M7CsA9E >}}

One More Step Along the World I Go, play along, 
by Sydney Carter 1971, 
lyrics, chords, sheet music, chromatic Harmonica Tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/12/one-more-step-along-the-world-i-go.pdf)
