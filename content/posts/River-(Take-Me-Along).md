---
title: River (Take Me Along)
date: 2018-02-18T10:39:06Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/cPa3mZL8wGc.jpg
categories:
 -
tags:
 -
---

{{< youtube cPa3mZL8wGc >}}

River  by Bill Staines 
Play Along 
Lyrics, Chords, Sheet Music, Chromatic harmonica tabs 
https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/river.pdf)
