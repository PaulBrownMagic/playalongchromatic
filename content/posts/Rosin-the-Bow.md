---
title: Rosin the Bow
date: 2020-08-07T03:05:24Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/RtW9AQKKiJI.jpg
categories:
 -
tags:
 -
---

{{< youtube RtW9AQKKiJI >}}

Rosin the Beau, play along, 
Traditional folk song, 
lyrics, chords, sheet  music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/08/rosin-the-beau.pdf)
