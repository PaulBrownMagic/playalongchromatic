---
title: The Broom O’ the Cowdenknowes
date: 2018-09-29T07:34:22Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/2kbzBp-1oSo.jpg
categories:
 -
tags:
 -
---

{{< youtube 2kbzBp-1oSo >}}

The Broom O' the Cowdenknowes, play along
lyrics, chords, sheet music, chromatic harmonica tabs
Free PDF of the music  can be downloaded here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/09/the-broom-o-the-cowdenknowes.pdf)
