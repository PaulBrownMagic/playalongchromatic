---
title: The Wild Geese (Norland Wind)
date: 2019-05-04T03:33:32Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/eDh3rAZ79pA.jpg
categories:
 -
tags:
 -
---

{{< youtube eDh3rAZ79pA >}}

Wild Geese (Norland Wind), play along,
lyrics, chords, sheet music, chromatic harmonica tabs

Free PDF:  https://playalongmusic.files.wordpress.com/2019/05/the-wild-geese-norland-wind.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/05/the-wild-geese-norland-wind.pdf)
