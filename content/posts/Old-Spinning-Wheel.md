---
title: Old Spinning Wheel
date: 2020-08-08T03:32:17Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/tlMnNTDoq0Y.jpg
categories:
 -
tags:
 -
---

{{< youtube tlMnNTDoq0Y >}}

Old Spinning Wheel
words and music by Billy Hill,
lyrics, chords, sheet music, chromatic harmonica tabs
More songs here:    https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2020/08/old-spinning-wheel.pdf)
