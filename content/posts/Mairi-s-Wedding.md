---
title: Mairi’s Wedding
date: 2018-01-15T01:33:53Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/pkzgJpmMns0.jpg
categories:
 -
tags:
 -
---

{{< youtube pkzgJpmMns0 >}}

Mairi's Wedding   play along
Key of C
lyrics, chords, sheet music, chromatic Harmonica tabs
Downloadable PDF available here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/02/mairis-wedding.pdf)
