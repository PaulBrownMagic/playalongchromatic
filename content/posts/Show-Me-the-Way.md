---
title: Show Me the Way
date: 2019-05-17T22:09:15Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/hL2GkgI-P4I.jpg
categories:
 -
tags:
 -
---

{{< youtube hL2GkgI-P4I >}}

Show Me the Way, play along, 
by Dennis DeYoung, 
lyrics, chords, sheet music, chromatic harmonica tabs

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2019/05/show-me-the-way.pdf)
