---
title: The Keeper
date: 2018-04-30T21:30:24Z
author: Easy Play-Along Music Videos
cover: /playalongchromatic/img/J29rrynd0lQ.jpg
categories:
 -
tags:
 -
---

{{< youtube J29rrynd0lQ >}}

The Keeper Did a Hunting Go, play along
lyrics, chords, sheet music, chromatic harmonica tabs
PDF here:  https://playalongmusic.wordpress.com/

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/04/the-keeper.pdf)
