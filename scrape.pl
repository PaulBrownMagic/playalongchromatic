:- use_module(library(http/http_open), [http_open/3]).
:- use_module(library(http/http_client), [http_get/3]).
:- use_module(library(xpath)).
:- use_module(library(http/json)).

:- prolog_load_context(directory, Dir),
  asserta(user:file_search_path(app, Dir)),
  asserta(user:file_search_path(img, app('static/img'))),
  asserta(user:file_search_path(posts, app('content/posts'))).

:- dynamic(html/1).
:- dynamic(json_resp/2).


youtube_api_key('AIzaSyBCLlxRYfxjLq-WADD3N4yPJjdacd-MUFg').
youtube_api_url("https://www.googleapis.com/youtube/v3/videos?part=snippet&id=~w&key=~w").

download_html :-
    \+ html(_),
	http_open('https://playalongmusic.wordpress.com/', Stream, []),
	load_html(stream(Stream), HTML, []),
	asserta(html(HTML)).

redownload_html :-
    retractall(html(_)),
	download_html.

youtube_api_call(URL, Data) :-
	youtube_url_id(URL, ID),
	youtube_api_key(Key),
	youtube_api_url(FAPI),
	format(atom(API), FAPI, [ID, Key]),
	setup_call_cleanup(
	    http_open(API, In, [ request_header('Accept'='application/json')
						   ]),
		json_read_dict(In, Data),
		close(In)
	).

cached_youtube_data(URL, Data) :-
	json_resp(URL, Data) ;
	\+ json_resp(URL, _),
	youtube_api_call(URL, Data),
	assertz(json_resp(URL, Data)).

strip_title(Char, Title, Stripped) :-
	atom_chars(Title, TC),
	once(strip_title_(Char, TC, SC)),
	atom_chars(Stripped, SC).
strip_title_(Char, [Char], []).
strip_title_(_, [], []).
strip_title_(Char, [Char|R], O) :-
	strip_title_(Char, R, O).
strip_title_(Char, [C|R], [C|O]) :-
	strip_title_(Char, R, O).

scrape_tune(Title, Video, Dots) :-
    html(HTML),
	xpath(HTML, //tr, TR),
	dif(A1, A2),
	xpath(TR, //a(@href=Video), A1),
	xpath(TR, //a(@href=Dots), A2),
	Video @> Dots,
	A1 = element(a, _, [T|_]),
	strip_title('\240\', T, Title).

tune_set(Tunes) :-
    setof(tune(Title, Video, Dots), scrape_tune(Title, Video, Dots), Tunes).

youtube_url_id(URL, ID) :-
	atom_concat('https://youtu.be/', ID, URL).
page_slug(Title, Slug) :-
	atom_chars(Title, Chars),
	once(replace(Chars, ' ', '-', SlugChars)),
	once(replace(SlugChars, '’', '-', SlugChars2)),
	once(strip_title_('?', SlugChars2, SlugChars3)),
	append(SlugChars3, ['.', 'm', 'd'], SlugFile),
	atom_chars(Slug, SlugFile).

replace([], _, _, []).
replace([From|Rest], From, To, [To|Out]) :-
	replace(Rest, From, To, Out).
replace([Char|Rest], From, To, [Char|Out]) :-
	Char \= From,
	replace(Rest, From, To, Out).

extract_youtube_data(Data, Title, Description, Date, ImgUrl) :-
	[Item|_] = Data.items,
	Snippet = Item.snippet,
	Title = Snippet.title,
	Description = Snippet.description,
	Date = Snippet.publishedAt,
	ImgUrl = Snippet.thumbnails.medium.url.

youtube_data(URL, Description, Date, ImgUrl) :-
	cached_youtube_data(URL, Data),
	extract_youtube_data(Data, _, Description, Date, ImgUrl).

write_no_clobber(Title, ID, Description, Date, Dots) :-
	page_slug(Title, Slug),
	absolute_file_name(posts(Slug), FileName),
	( exists_file(FileName)
    ; \+ exists_file(FileName),
      write_and_clobber(Title, ID, Description, Date, Dots)
    ).

write_and_clobber(Title, ID, Description, Date, Dots) :-
	page_slug(Title, Slug),
	absolute_file_name(posts(Slug), FileName),
    setup_call_cleanup(
	    open(FileName, write, FileStream),
		write_to_file(Title, ID, Description, Date, Dots, FileStream),
		close(FileStream)
	).

write_to_file(Title, ID, Description, Date, Dots, FileStream) :-
	write(FileStream, '---\ntitle: '), write(FileStream, Title),
	write(FileStream, '\ndate: '), write(FileStream, Date),
	%write(FileStream, '\nlastmod: '), write(FileStream, Date),
    write(FileStream, '\nauthor: Easy Play-Along Music Videos\ncover: /img/'),
	write(FileStream, ID), write(FileStream, '.jpg\n'),
	write(FileStream, 'categories:\n -\ntags:\n -\n---\n\n'),
	write(FileStream, '{{< youtube '), write(FileStream, ID), write(FileStream, ' >}}\n\n'),
	atom_chars(Description, DC), replace(DC, '\r', ' ', DSN), atom_chars(SDescription, DSN),
	write(FileStream, SDescription),
	write(FileStream, '\n\n[Download Sheet Music]('), write(FileStream, Dots), write(FileStream, ')\n').

update_all :-
	tune_set(Tunes),
     forall(member(Tune, Tunes),
	     update_tune(Tune)).

write_all :-
	tune_set(Tunes),
     forall(member(Tune, Tunes),
	     write_tune(Tune)).

download_img(ImgUrl, ID) :-
	atom_concat(ID, '.jpg', FileName),
	absolute_file_name(img(FileName), FilePath),
	( exists_file(FilePath)
    ; \+ exists_file(FilePath),
		format(atom(Cmd), "wget -O ~w ~w", [FilePath, ImgUrl]),
		shell(Cmd)
	).

write_tune(tune(Title, Video, Dots)) :-
	youtube_data(Video, Description, Date, ImgUrl),
	youtube_url_id(Video, ID),
	write_and_clobber(Title, ID, Description, Date, Dots),
    download_img(ImgUrl, ID).

update_tune(tune(Title, Video, Dots)) :-
	youtube_data(Video, Description, Date, ImgUrl),
	youtube_url_id(Video, ID),
	write_no_clobber(Title, ID, Description, Date, Dots),
    download_img(ImgUrl, ID).
