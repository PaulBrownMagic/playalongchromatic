---
title: A Light is Gleaming
date: 2018-12-27T22:48:07Z
author: Easy Play-Along Music Videos
cover: /img/HN1Vhv2BEtE.jpg
categories:
 -
tags:
 - Hymns
---

{{< youtube HN1Vhv2BEtE >}}

A Light is Gleaming, play along
by Linnea Good
lyrics, chords, sheet music, chromaic harmonica tabs
PDF:  https://playalongmusic.files.wordpress.com/2018/12/A-Light-is-Gleaming.pdf

[Download Sheet Music](https://playalongmusic.files.wordpress.com/2018/12/A-Light-is-Gleaming.pdf)
